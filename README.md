<p align="center">
  <a href="https://etecitapira-astronomia.netlify.app">
    <img alt="Logo" src="https://imgur.com/XMP9xKH.png" width="60" />
  </a>
</p>
<h1 align="center">
  Projeto Astronomia
</h1>

<p>Projeto realizado em 2022 na matéria de física.</p>
<p>Desenvolvido por @MysteryMS e @LucasGabrielTheodoro
<p>Artigos desenvolvidos pelos alunos do segundo ano de Desenvolvimento de Sistemas</p>

<h2 align="center">
Com o Apoio de
</h2>

<h3>IDEs</h3>
<div style="align-items: center; display: flex;">
<a href="https://www.jetbrains.com/webstorm/">
<img width="45" src="https://resources.jetbrains.com/storage/products/company/brand/logos/WebStorm_icon.svg" alt="Webstorm Logo">
</a>
<p style="margin-left: 10px">Webstorm</p>
</div>
<div style="align-items: center; display: flex;">
<a href="https://code.visualstudio.com/">
<img width="45" src="static/vscode.svg" alt="VSCode Logo">
</a>
<p style="margin-left: 10px">Visual Studio Code</p>
</div>

<br/>

<h3>Frameworks</h3>
<div style="align-items: center; display: flex">
<a href="https://www.gatsbyjs.com/">
<img alt="Gatsby Logo" width="45" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg"/>
</a>
<p style="margin-left: 10px">Gatsby</p>
</div>

<br/>

<h3>Web Hosting</h3>
<div style="display: flex; align-items: center">
<a href="https://www.netlify.com/">
<img width="45" alt="Netlify Logo" src="static/netlify.svg">
</a>
<p style="margin-left: 10px">Netlify</p>
</div>
