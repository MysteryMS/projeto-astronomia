import * as React from "react"
import Navbar from "../components/Navbar"
import "../index.css"
import {Container, Grid, Link, NextUIProvider, Row, Spacer} from "@nextui-org/react"
import {theme} from "../Theme"

const About = () => {
  return (
    <main>
      <title>Sobre o site</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify="center" xs={12}>
              <h1>Mais Sobre o Site</h1>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p style={{textAlign: 'justify'}}>A construção do site de Astronomia foi realizada pelos alunos Pedro Piva e Lucas Gabriel. Todos os artigos utilizadas neste site foram elaborados pelos demais alunos da sala do 2°C do ano de 2022. </p>
              <p style={{textAlign: 'justify'}}>Este trabalho compõe a avaliação do bimestre da disciplina de física sobre a orientação da professora Alessandra Balbino da escola Etec João Maria Stevanatto. </p>
              <Row align={'center'}>
                <img src={'/gitlab.svg'} width={45} alt={'gitlab logo'}/>
                <p><Link href={'https://gitlab.com/MysteryMS/projeto-astronomia'}>Visite o projeto</Link> no GitLab</p>
              </Row>
            </Grid>

            <Grid xs={12}>
              <h2>Projeto Realizado com o Apoio de</h2>
            </Grid>

            <Grid xs={12} md={4} direction={'column'} justify={'center'} alignItems={'flex-start'}>
              <h3>IDEs</h3>
              <Row align={'center'} justify={'flex-start'}>
                <img src={'https://resources.jetbrains.com/storage/products/company/brand/logos/WebStorm_icon.svg'}
                     alt={'webstorm icon'} width={45}/>
                <Spacer x={0.5}/>
                <p>WebStorm</p>
              </Row>
              <Row align={'center'}>
                <img src={'/vscode.svg'} width={45} alt={'vscode icon'}/>
                <Spacer x={0.5}/>
                <p>Visual Studio Code</p>
              </Row>
            </Grid>

            <Grid xs={12} md={4} direction={'column'}>
              <h3>Frameworks</h3>
              <Row align={'center'}>
                <img src={'/icon.png'} alt={'gatsby icon'} width={45}/>
                <Spacer x={0.5}/>
                <p>Gatsby</p>
              </Row>
            </Grid>

            <Grid xs={12} md={4} direction={'column'}>
              <h3>Web Hosting</h3>
              <Row align={'center'}>
                <img src={'/netlify.svg'} alt={'netlify icon'} width={45}/>
                <Spacer x={0.5}/>
                <p>Netlify</p>
              </Row>
            </Grid>

            <Grid xs={12} direction={'column'}>
            <p style={{textAlign: 'justify'}}>No total foram criados 29 artigos conforme listados a baixo seguido pelo nome do(s) aluno(s) responsáveis:</p>
            <ul>
              <li>•	"É errado Chamar uma Estrela de Cadente?" – Luís Gustavo;</li>
              <li>•	"Como Surgiu o Big Bang" - Helano Rangel;</li>
              <li>•	"Nebulosas" - Yasmin Sampaio;</li>
              <li>•	"Constelações" - Gustavo Brandão & Julia Ziola;</li>
              <li>•	"Telescópio Espacial Hubble" – João Pedro Pesuto;</li>
              <li>•	"Possibilidade de Vida em Outros Planetas" – Carlos Alberto;</li>
              <li>•	"Planetas do Sistema Solar" – Murilo Cruz;</li>
              <li>•	"Aurora Boreal" - Emanuelle Vicente & Elen Marissa;</li>
              <li>•	"Kepler-186f" - Fellipe Franco;</li>
              <li>•	"Galáxias" - Maria Eduarda Carvalho de Meneses;</li>
              <li>•	"Estrelas" - João Gabriel;</li>
              <li>•	"Cometas" - Bruno Rodrigues;</li>
              <li>•	"Curiosidades Gerais Sobre Buracos Negros" - João Raposo;</li>
              <li>•	"Os Maiores Asteroides do Sistema Solar" - Nicolas Rossini Gill;</li>
              <li>•	"Astronomia X Astrologia" - Cauê Vinicius & Aline Mattioli;</li>
              <li>•	"Lua" - Victor Hugo Sousa de Oliveira;</li>
              <li>•	"Astronomia na Mídia" - Pedro Henrique;</li>
              <li>•	"Quasar" - Felipe Ricardo & Victor Brait;</li>
              <li>•	"Sol" - Leonardo de Paiva;</li>
              <li>•	"Constelações" - Gustavo Brandão & Julia Ziola;</li>
              <li>•	"Urano" - Gabriel Soares Campestrini;</li>
              <li>•	"Arco-Íris" - Vinicius Rocha de Vasconcellos;</li>
              <li>•	"Aglomerado de Galáxias" - Paola Lavigni, Julia Oliveira & Amanda Jullye;</li>
              <li>•	"O Vácuo do Universo" - Samuel Reis, Gustavo Telini & Diogo Henrique;</li>
              <li>•	"Andrômeda (Galáxia)" - Paola Lavigni Zacariotto;</li>
              <li>•	"Qual a Maior Estrutura do Universo?" - Alan Viderman da Silva;</li>
              <li>• "Eclipses" - Felipe Pelizzer;</li>
              <li>• "Influência da Astronomia" - Sofia Loren;</li>
              <li>• "Júpiter" - Tiago Costa & Otávio Goulart;</li>
            </ul>
            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default About


