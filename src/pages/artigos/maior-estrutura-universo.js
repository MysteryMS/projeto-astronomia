import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const maiorEstruturaUniverso = () => {
  return (
    <main>
      <title>Qual a Maior Estrutura do Universo?</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Qual a Maior Estrutura do Universo?</h1>
              <ArticleAuthor name={'Alan Viderman da Silva'} image={'/pfps/alan.jpg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>QUAL SERÁ A MAIOR ESTRUTURA DO UNIVERSO? UMA galáxia? Um buraco negro supermassivo? Nenhum deles. A
                maior estrutura já detectada no Universo é o Boss Great Wall, um aglomerado de 830 galáxias descoberto
                por uma equipe do Instituto de Astrofísica das Ilhas Canárias.</p>
              <p>A estrutura possui 1 bilhão de anos-luz de diâmetro — esse agrupamento é 10 mil vezes maior que a Via
                Láctea! Ela é conectada por gases e, graças à gravidade, seus componentes se agitam e se movimentam em
                conjunto pelo vácuo do espaço. Essa estrutura se encontra a cerca de 4,5 a 6,5 bilhões de anosluz da
                Terra. Alguns cientistas contestam o fato de o Boss Great Wall ser a maior estrutura já encontrada no
                Universo, argumentando que esses superaglomerados não estão realmente conectados. Em vez disso, possuem
                lacunas mais ou menos ligadas por nuvens de gás e poeira. Mas esse tipo de discussão sempre ocorre
                quando tais objetos são encontrados; no final das contas, os argumentos parecem resumir-se a definições
                pessoais do que constitui uma estrutura singular, com a maioria dos pesquisadores concordando que tais
                aglomerados são uma coisa só. Contudo uma coisa é certa: dá um nó na cabeça tentar calcular o tamanho
                desse aglomerado. </p>
              <p>E é com esse objeto monstruoso que finalizamos nossa viagem pelo Universo. Nesta jornada você viu que
                somos menores do que um pixel nessa gigantesca imagem em Hp que é o cosmos. Notou que, mesmo que nossos
                instrumentos tenham possibilitado a descoberta de outros mundos que podem potencialmente abrigar vida,
                como Proxima B, a Terra é o melhor lugar para receber a vida que conhecemos e, apesar de pequena e
                insignificante perto de objetos enormes como galáxias e buracos negros, é o lugar mais especial deste
                Universo, pois é a nossa casa. Por mais que um dia seja possível colonizar outros planetas (em uma visão
                bem otimista, pois levando em consideração a forma desenfreada com que estamos acabando com os nossos
                recursos, pode ser que não cheguemos tão longe), nenhum deles será tão convidativo para nós quanto
                este.</p>
              <p>Estamos vagando junto com ela pelo grande vazio, perdidos e tentando nos localizar dentro do tempo e do
                espaço, caímos de paraquedas na existência, e as regras dela estão decodificadas pela natureza. Passamos
                séculos juntando cada símbolo desse código tentando dar sentido à linguagem que o Universo fala. Se
                teremos tempo para entender as regras do jogo eu não sei, ou será que já as entendemos? Pois, segundo
                Albert Einstein, o passado, o presente e o futuro coexistem; então tudo o que fomos, somos e seremos já
                existe no tecido do espaço-tempo. Estamos inseridos em um filme cósmico com começo, meio e fim. Nossa
                consciência é o olho do espectador assistindo a esse longa-metragem que, no momento, está em seus 13,8
                bilhões de anos de duração. Ao menos do nosso ponto de vista, somos os protagonistas e cabe a nós
                absorver as melhores informações, fazer as melhores escolhas, tornar o “roteiro” do nosso papel mais
                encorpado, evitando ser um mero figurante na obra. Podemos contribuir para que pelo menos a nossa
                participação tenha um desfecho feliz, ou pode ser que já tivemos, em algum lugar do espaço-tempo...</p>
            </Grid>

            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/maior-estrutura-do-universo.webp'} width={400} showSkeleton={false} objectFit={'cover'}/>
            </Grid>


          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default maiorEstruturaUniverso
