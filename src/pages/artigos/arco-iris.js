import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const arcoiris = () => {
  return (
    <main>
      <title>Arco-Íris</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItens={'center'}>
              <h1>Arco-Íris</h1>
              <ArticleAuthor name={'Vinicius Rocha de Vasconcellos'}/>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>O que é?</h2>
              <p>O arco-íris é um dos fenômenos que forma pela decomposição da luz branca em algumas cores. Essa separação das cores acontece em gotículas d’água. Então é por esse motivo que normalmente vemos o arcoíris quando está chovendo, ou após o fim da chuva. </p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/arco-iris.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>Descoberta do arco-íris: </h2>
              <p>Isaac Newton descobriu usando um prisma triangular, atravessou sobre ele um brilho de luz branca, e com isso aparece as 7 cores visíveis. Seguindo este mesmo raciocínio, ele concluiu que a luz do Sol quando atravessa as gotículas de água, acontece o famoso arco-íris  </p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/Isaac-Newton.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>Curiosidades: </h2>
              <p>-	O arco-iris não é formado apenas pelas 7 cores tradicionais: vermelho, laranja, amarelo, verde, azul, azulescuro e violeta. </p>
              <p>-	Na verdade, as cores são infinitas, porém, o nosso olho humano consegue identificar apenas 7 cores do Arco-íris. </p>
              <p>-	Existia uma Deusa do arco-íris na Mitologia Grega chamada Íris conhecida como mensageira dos deuses do Olimpo. </p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/Deusa-do-arco-iris.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>


          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default arcoiris
