import * as React from "react"
import Navbar from "../../components/Navbar"
import ArticleAuthor from "../../components/ArticleAuthor"
import "../../index.css"
import {Container, Grid, NextUIProvider, Row, Spacer} from "@nextui-org/react"
import {theme} from "../../Theme"
import StarWarsCard from "../../components/StarWarsCard"
import PerdidosEspacoCard from "../../components/PerdidosEspacoCard"
import AstroneerCard from "../../components/AstroneerCard"
import UniverseSandboxCard from "../../components/UniverseSandboxCard"
import DeadSpaceCard from "../../components/DeadSpaceCard"
import DispatchMusicaCard from "../../components/DispatchMusicaCard"
import Badge from "../../components/Badge"


const AstronomiaNaMidia = () => {
  return (
    <main>
      <title>Astronomia na Mídia</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={`center`} direction={'column'}>
              <h1>Astronomia na Mídia</h1>
              <ArticleAuthor name={'Pedro Henrique'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>A quantidade de livros, séries, filmes, músicas e muitos outros estilos de mídia, que utilizam da
                astronomia, como objeto de criação de universo, ou até mesmo como foco central de sua história, é
                absurdamente enorme.</p>
              <p> A astronomia é cativante, o estudo dos planetas, das estrelas, buracos negros, é impressionante, mas
                ao mesmo tempo assustador. Em um universo incompreensivelmente grande, o que nos espera? Vida
                inteligente? Será que estamos sozinhos? Somos apenas uma simples bolinha de pedra e água flutuando ao
                redor de uma estrela? Possuímos algum propósito maior? Talvez nunca saibamos..., mas, podemos imaginar,
                inventar histórias, contá-las. E é sobre estas histórias que vamos falar hoje.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Star Wars</h2>
                <Spacer x={1}/>
                <Badge text={'Trilogia de Filmes'} color={'#21408f'}/>
              </Row>
              <Grid direction={'column'}>
                <StarWarsCard/>
              </Grid>
              <p>Talvez, não, com certeza a primeira coisa que vem a mente das pessoas quando falamos de astronomia e
                mídia combinadas. Mesmo não sendo exatamente, preciso, com muitas leis da física.</p>
              <p>Star Wars não roda em torno da astronomia, mas sim ao redor dos personagens e de suas histórias,
                facilmente abandonando características científicas. Um dos tópicos mais levantados sobre esse assunto, é
                o de que, existe muito som no espaço. Tiros, explosões, motores das naves, tudo isso faz som, mesmo,
                estando no vácuo do espaço.</p>
              <p>O próprio criador, George Lucas, disse em uma entrevista que, “Star Wars está mais para um romance de
                fantasia, do que para ficção científica”, fazendo que o realismo seja deixado de lado, para que história
                fique mais divertida e cativante.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Perdidos no Espaço</h2>
                <Spacer x={1}/>
                <Badge text={'Série'} color={'#881751'}/>
              </Row>
              <Grid direction={'column'}>
                <PerdidosEspacoCard/>
              </Grid>
              <p>É uma série, na qual acompanhamos uma família, que faz parte de um grande grupo de exploradores,
                tentando alcançar Alpha Centauri (o sistema mais próximo de nosso Sistema Solar). Se passando num futuro
                próximo, onde a Terra já foi explorada e abusada ao máximo, onde até tentamos consertar nossos erros,
                mas já era tarde. Muitos imprevistos são encontrados nesta viagem, fazendo que nossos protagonistas se
                encontrem, perdidos... no... espaço, nome bem alto explicativo.</p>
              <p>Diferentemente de Star Wars, Perdidos no espaço, tenta utilizar o máximo de fatores científicos, se
                baseando em muitas coisas que sabemos que são reais. Alpha Centauri, e o planeta para qual estão
                viajando, Proxima Centauri B, realmente existem, e são considerados a alternativa mais “viável” para uma
                possível “ocorrência” futura. </p>
              <p>Mas, devemos lembrar que, tecnologicamente falando, ainda estamos muito longe de alcançar o nível de
                viagem espacial demonstrada na série. Por isso devemos cuidar, e preservar o nosso planeta da melhor
                forma possível, pois para nossa geração, e muitas outras ainda por vir, uma viagem interplanetária de
                tal magnitude, não é uma opção.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Astroneer</h2>
                <Spacer x={1}/>
                <Badge text={'Jogo'} color={'#772a14'}/>
              </Row>
              <Grid direction={'column'}>
                <AstroneerCard/>
              </Grid>
              <p>Astroneer é um jogo de exploração, onde você, sozinho ou com amigos, é enviado para explorar os
                planetas e seus segredos, em um pequeno sistema solar bem parecido com o nosso.</p>
              <p>Ao pousar em seu primeiro planeta, você perceberá que as coisas não serão tão simples, já que todos os
                itens que você recebe são: Um abrigo, um gerador de oxigênio e um gerador de energia. Tudo que você for
                precisar, você terá que encontrar e extrair dos planetas. </p>
              <p>Cada planeta possuí: Uma variedade diferente de recursos, uma flora única e climas próprios, todos
                esses fatores são definidos pela distância da estrela no centro do sistema, e pela quantidade de luz que
                cada planeta recebe.</p>
              <p>O jogo possuí muitos recursos naturais, sendo uma grande maioria deles recursos reais, que podem ser
                encontrados, ou produzidos a partir de outros recursos. Alguns deles são: Argila, quartzo, esfarelita,
                malaquita, volframita, hematita, titanita.</p>
              <p>É um jogo simples, graficamente falando, mas, extremamente interessante possuindo muitos aspectos
                realísticos para um jogo.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Universe Sandbox 2</h2>
                <Spacer x={1}/>
                <Badge text={'Jogo'} color={'#772a14'}/>
                <Spacer x={0.5}/>
                <Badge text={'Simulador'} color={'#0c9a84'}/>
              </Row>
              <Grid direction={'column'}>
                <UniverseSandboxCard/>
              </Grid>
              <p>Universe Sandbox é, com certeza o simulador espacial mais realista, e divertido que temos a nossa
                disposição. Tudo que acontece dentro do “jogo” é calculado em tempo real, seguindo todas as leis da
                física possíveis.</p>
              <p>Porém, não seria divertido apenas assistir os planetas girando ao redor Sol. Nós podemos alterar a
                massa, temperatura, velocidade, órbita, e até adicionar ou remover planetas. O que aconteceria se o Sol
                desaparecesse? E se a massa de Júpiter fosse 8x maior? Nesse simulador essas perguntas, e muitas outras
                poderiam ser respondidas, não apenas de forma científica, mas também interativa e divertida.</p>
              <p>O simulador ainda está em fase de desenvolvimento, por isso pode apresentar algumas falhas, mas devemos
                dar créditos aos criadores, que provavelmente, tiveram uma extensa aula de astronomia e física só para
                terem uma ideia de como construí-lo. </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Dead Space</h2>
                <Spacer x={1}/>
                <Badge text={'Jogo'} color={'#772a14'}/>
              </Row>
              <Grid direction={'column'}>
                <DeadSpaceCard/>
              </Grid>
              <p>Dead Space é uma série de jogos que se passa em um futuro distante, onde todos os recursos da Terra já
                foram explorados, e muitas colônias humanas estão espalhadas pela galáxia. </p>
              <p> Acompanhamos a história de Isaac Clarke, um engenheiro que estava a caminho de uma estação que tinha
                emitido um sinal de socorro. O objetivo dessa estação era a exploração dos recursos naturais de um
                planeta, para o abastecimento da Terra e suas colônias. Isaac e sua equipe tentam se comunicar com a
                estação, porém não recebem nenhum sinal de volta.</p>
              <p>Ao abordar a estação nenhum sinal de vida está presente. Quando a equipe tenta descobrir o que está
                errado, dois de seus integrantes são atacados pelo que parece ser um monstro, morrendo no processo.
                Viemos a descobrir que esse monstro, é na verdade, um ser humano contaminado com um parasita
                alienígena. </p>
              <p>Nosso principal objetivo nos jogos é sobreviver, e tentar parar a contaminação. O jogo mesmo se
                passando no espaço, não aborda muitos aspectos astronômicos, sendo mais parecido com Star Wars nesse
                quesito, focando-se mais na história e em seus personagens, quebrando muitas leis da física no processo.
                Como a viagem mais rápida do que a luz, que sabemos ser algo impossível de se alcançar na vida
                real. </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Row align={'center'}>
                <h2>Dispatch “Circles Around the Sun”</h2>
                <Spacer x={1}/>
                <Badge text={'Música'} color={'#af1111'}/>
              </Row>
              <Grid direction={'column'}>
                <DispatchMusicaCard/>
              </Grid>
              <p>Muitas músicas gostam de falar sobre os astros e sua beleza, algumas de formas mais... “inortodoxas”
                (não é mesmo Paula Fernandes?), mas eu escolhi esta música em específico por conta da falta de seu “meio
                lírico”. Essa diferentemente de muitas outras músicas, canta uma história, (que tem sua veracidade
                debatida até hoje) a história de um homem, de um garoto, que com apenas doze anos foi lançado ao
                espaço.</p>
              <p> Esse garoto, (de nome não identificado na música) seria um deficiente, que quando nasceu, chocou os
                médicos, que em seguida fariam uma intensa bateria de testes com ele. Quando tivesse completado doze
                anos, o governo utilizaria de sua deficiência, sendo ele menor e mais leve que uma criança comum, mas
                também incapaz de falar e se locomover por conta própria, e o lançariam ao espaço. De acordo com a
                música, ele teria adorado a viagem, e (deixado implícito na música) retornaria a fazer mais viagens
                orbitais.</p>
              <p>A veracidade dessa história é altamente debatida, por conta de que nada em relação a esse programa
                espacial foi revelado, ou sequer mencionado pelo governo estado-unidense, deixando um grande espaço para
                teorias surgirem.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>A astronomia atualmente, está implantada em nossas vidas, sendo impossível viver sem conhecê-la, ou,
                ouvir falar dela. Além de ser um grande objeto de estudos, é também, um grande motivador da criação,
                pois, a única maior que toda essa imensidão intergaláctica, é a nossa imaginação.</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default AstronomiaNaMidia
