import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, Link, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

const VidasPlanetarias = () => {
  return (
    <main>
      <title>Possibilidade de Vida em Outros Planetas</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Possibilidade de Vida em Outros Planetas</h1>
              <ArticleAuthor name={"Carlos Alberto"} image={'/pfps/carlos.jpeg'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>O chefe da Nasa, <Link href="https://epocanegocios.globo.com/palavrachave/nasa/"> Bill
                Nelson</Link>, disse que provavelmente não estamos sozinhos e que há vida fora da
                <Link underline href="https://epocanegocios.globo.com/palavrachave/terra/"> Terra</Link>. Segundo ele, a
                grandeza do universo abriga alguns mistérios.</p>
              <p>“Há até teorias de que podem haver outros universos. Se esse for o caso, quem sou eu para dizer que o
                planeta Terra é o único local com uma forma de vida civilizada e organizada como a nossa? ”, questionou
                ele em entrevista ao Centro de Política da Universidade da Virgínia, nos Estados Unidos. “Existem outros
                planetas Terra lá fora? Acho que sim, porque o universo é muito grande”, complementou.</p>
              <p>Nelson destacou na entrevista que a busca por vida extraterrestre é um dos focos de explorações da
                Nasa. Além disso, afirmou que pilotos da Marinha já relataram mais de 300 avistamentos de objetos
                voadores não identificados (<b>OVNIs</b>) desde 2004.</p>
              <p>“Eles não sabem o que é, nós não sabemos o que é. Então, esta é uma coisa que estamos constantemente
                procurando”, comentou. O objetivo das missões é responder a algumas perguntas sobre a forma que os
                humanos chegaram à Terra, como foram civilizados, entre outros, de acordo com Nelson.</p>
              <p>“O que acha que estamos fazendo em <Link underline
                                                          href="https://epocanegocios.globo.com/palavrachave/marte/"
              >Marte</Link>? Estamos procurando vida. Isso faz
                parte da missão da Nasa”,
                salientou ele. Para o chefe da agência espacial, a possibilidade de haver vida inteligente em outros
                lugares do universo reforça a importância de cuidarmos do nosso planeta e da forma como nos relacionamos
                uns com os outros. </p>
            </Grid>

            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/planetas.webp'} showSkeleton={false}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <h2>Mas Quais Seriam os Requisitos para um Planeta Possuir Vida?</h2>
              <p>Para existir vida em outros planetas é necessário que o planeta esteja a uma distância aceitável da
                estrela para que haja temperaturas aceitáveis entre -50ºC e 40ºC.</p>

              <h2>Composição da Atmosfera</h2>
              <p>É necessário que a atmosfera neste planeta tenha uma composição rica assim como a nossa, pois somente
                oxigênio pode ser tóxico para alguns tipos de vida mais desenvolvidas (assim como seres humanos).</p>

              <h2>Água no Estado Líquido</h2>
              <p>Este fator é essencial, pois água é muito utilizado como forma de se conduzir nutrientes pelo corpo de
                praticamente todos os seres vivos.
                (Lembrando que essas são as condições necessárias para que nós e os seres vivos do nosso planeta
                existam, não podemos dizer que isso se aplica a uma forma de vida extraterrestre).</p>

              <h2>Possíveis Planetas com Vida</h2>
              <p>Esses são alguns planetas que podem ter vida</p>

              <ul>
                <li>• Kepler 186f;</li>
                <li>• TRAPPIST-1d;</li>
                <li>• Próxima Centauri b;</li>
                <li>• GJ 1132b;</li>
                <li>• LHS 1140b;</li>
              </ul>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/alien-em-jupiter.webp'} showSkeleton={false}/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default VidasPlanetarias
