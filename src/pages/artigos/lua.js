import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const lua = () => {
  return (
    <main>
      <title>Lua</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItens={'center'}>
              <h1>Lua</h1>
              <ArticleAuthor name={'Victor Hugo Sousa de Oliveira'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>A lua é um satélite natural da Terra.</p>
            </Grid>

            <Grid md={6} xs={12}>
              <Image src={'/lua.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} md={6} direction={'column'} alignItems={'center'} justify={'center'}>
              <p><b>Massa:</b> 7,35.1022 kg </p>
              <p><b>Diâmetro:</b> 3.474,8 km </p>
              <p><b>Gravidade:</b> 1,62 m/s² </p>
              <p><b>Distância da Terra:</b> 384.400 km </p>
              <p><b>Período orbital:</b> 27 dias </p>
              <p><b>Idade:</b> 4,53.109 anos   </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Origem</h2>
              <p>A teoria mais aceita pelos astrônomos sobre a origem da Lua é que há 4,5 bilhões de anos, dois corpos celeste se colidiram formando
                  a Lua, um corpo tinha a dimensão parecida com a de Marte e a outra dimensão parecida com a da Terra.</p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/impacto-da-lua.webp'} showSkeleton={false} width={500}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Ciclo Lunar </h2>
              <p>A Lua possui um ciclo chamado ciclo lunar, este tem  quatro fases que tem duração de 7 a 8 dias, o ciclo total tem entre 29 e 30 dias, sendo elas:  .</p>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Lua Nova </b> Nessa fase, a Lua se encontra entre a Terra e o Sol, portanto ela está em conjunção com o Sol.
              Nessa altura á face não iluminada da Lua está virada para Terra de modo que não e visível a olho nu. </p>
            </Grid>
            <Grid md={6} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/luanova.webp'} showSkeleton={false}/>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Lua Crescente:</b> Nessa fase a Lua ele vai crescendo como o próprio nome dela fala “Crescente” até a lua ficar cheia, para
              conseguirmos enxergá-la totalmente.  </p>
            </Grid>
            <Grid md={6} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/luacrescente.webp'} showSkeleton={false}/>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Lua Cheia:</b> Ocorre quando a Terra está entre a Lua e o Sol. Nesse período os raios do Sol conseguem chegar totalmente na face
              da Lua; assim conseguimos enxergar a Lua toda.</p>
            </Grid>
            <Grid md={6} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/luacheia.webp'} showSkeleton={false}/>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Lua Minguante:</b> Nessa fase, a Lua tem um formato de um semicírculo, só que oposto do lado crescente da Lua, ela vai minguando
              até se tornar nova e começar um novo ciclo.</p>
            </Grid>
            <Grid md={6} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/luaminguante.webp'} showSkeleton={false}/>
            </Grid>

            <Grid direction={'column'}>
              <h2>Influência da Lua na maré </h2>
              <h3>Por que isso ocorre?</h3>
              <p>As marés ficam altas ou baixas de acordo com a força de gravidade da Lua; por exemplo, quando a Lua está cheia ou nova, as marés são mais altas porque a Lua está mais próxima da Terra assim exercendo mais força gravitacional. Já quando ela está crescente ou minguante, as marés estão mais baixas pois a Lua está mais afastada da Terra assim exercendo menos força gravitacional na Terra. </p>

              <h3>Isso causa problemas?</h3>
              <p>Até o presente momento os cientistas não falaram nada sobre isso ser um problema muito grande para a humanidade. </p>
            </Grid>
            <Grid direction={'column'}>
              <h2>E se a Lua não existisse?</h2>
              <ul>
                  <li>• As marés ficariam 1/3 mais fracas. Com isso somente o Sol exerceria influência nas marés. </li>
                  <li>•	A noção de “mês” deixaria de existir. Confusão total para contar o tempo, porque nós dividimos os anos em meses, que é o período que a Lua demora para dar uma volta completa na Terra. </li>
                  <li>•	A Terra giraria mais rápido. Com a velocidade mais rápida, os furacões se formariam mais rápido, causariam tempestades mais frequentes e mais violentas. </li>
                  <li>•	A gente envelheceria mais de pressa. Pense em um dia com 4 horas. Boa parte dos seres vivos dormem à noite; a alternância entre dia-noite-dia seria mais rápida, com essa aceleração isso faria nosso corpo trabalhar com maior rapidez. </li>
                  <li>•	A Data em que cada estação do ano começa, mudaria sempre. Pois o que determina as estações é a inclinação da Terra e quem deixa essa inclinação estável é a Lua; a instabilidade deixaria o clima maluco: um dia seria inverno, outro dia seria verão. </li>
                  <li>•	Não haveria influências místicas e folclóricas ligadas a Lua. Fim dos rituais, fim do romantismo ligado à Lua e fim das suas belas paisagens </li>
              </ul>

            </Grid>



          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default lua
