import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const urano = () => {
  return (
    <main>
      <title>Urano</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItens={'center'}>
              <h1>Urano</h1>
              <ArticleAuthor name={'Gabriel Soares Campestrini'}/>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>Características de Urano</h2>
              <p>Urano é um planeta gasoso localizado na sétima posição em relação ao Sol, logo após Saturno e anterior a Netuno. É o terceiro maior planeta do Sistema Solar e sua massa é formada por gases, principalmente hidrogênio e hélio. A sua nomenclatura homenageia o deus Urano da mitologia grega, que representava a personificação do céu. O planeta Urano é o único do Sistema Solar que recebeu um nome mitológico de origem grega, sendo os demais nomes dos planetas oriundos de deuses romanos.</p>
              <p>O planeta Urano tem a atmosfera mais fria do Sistema Solar, sendo caracterizada pelas baixas temperaturas que podem chegar a quase 200°C negativos. Ele é conhecido por sua coloração azulada. A movimentação de Urano, diferentemente de outros planetas do Sistema Solar, ocorre de leste para o oeste. Sendo assim, o Sol em Urano nasce no oeste e se põe no leste, movimento contrário ao realizado pelo planeta Terra.</p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/apenas-urano.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>Anéis de Urano</h2>
              <p>Os anéis de Urano foram identificados em 1977. Na época, a descoberta significou uma grande novidade para a astronomia, uma vez que, até então, acreditava-se que apenas Saturno possuía anéis. Na atualidade, sabe-se que todos os planetas gasosos (Júpiter, Saturno, Urano e Netuno) possuem anéis. A principal característica dos anéis de Urano é a opacidade, uma que vez ele não tem iluminação própria. O planeta Urano possui 13 anéis planetários conhecidos. Eles são estreitos e formados, possivelmente, por uma estrutura de partículas espaciais e até mesmo gelo.</p>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>Luas de Urano</h2>
              <p>Assim como os demais planetas do Sistema Solar, Urano possui um conjunto de satélites naturais, também chamados de luas. Na atualidade, são conhecidas 27 luas em Urano, sendo a última identificada em 1948, ela recebeu o nome de Miranda. As luas de Urano foram nomeadas em homenagem às personagens clássicas da literatura mundial, presentes em obras dos escritores William Shakespeare (1564-1616) e Alexander Pope (1688-1744).</p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/urano-no-espaço.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Exploração de Urano</h2>
              <p>O planeta Urano, em razão da sua distância do planeta Terra, da sua composição gasosa e das dificuldades técnicas de sua exploração, é um dos menos conhecidos do Sistema Solar. Ele foi identificado em 1781, pelo pesquisador alemão William Herschel (1738-1822), por meio da utilização de um telescópio. A sua descoberta foi um importante momento para a história da astronomia. Porém, desde então foram realizados poucos estudos em relação ao planeta. A única visita de um equipamento astronômico em Urano ocorreu em 1986, por meio de uma sonda chamada Voyager 2. O planeta deve ser explorado por novas missões em um futuro próximo.</p>
            </Grid>
            

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default urano
