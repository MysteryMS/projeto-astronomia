import * as React from 'react'
import '../../article.css'
import Navbar from '../../components/Navbar'
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"

const EstrelaCadente = () => {
  return (
    <main>
      <title>É Errado Chamar uma Estrela de Cadente?</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} alignContent={'center'} direction={'column'}>
              <h1>É Errado Chamar uma Estrela de Cadente?</h1>
              <ArticleAuthor name={'Luís Gustavo'}/>
            </Grid>

            <Grid xs={12}>
              <p style={{textAlign: 'justify', fontSize: '18px'}}>Em virtude da alta velocidade e do atrito que gera no
                ar-atmosférico, esses corpos do espaço incendeiam-se, o que provoca um rastro de luz no céu
                noturno. </p>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <h2>Nomenclatura</h2>
              <p>Dependendo de sua posição, os corpos celestes recebem nomes diferentes. Se estiverem vagando no espaço
                fora de nossa atmosfera, eles serão chamados de meteoroides; mas se entrarem na atmosfera terrestre e
                incendiarem-se por causa do contato com o ar atmosférico, eles serão denominados de meteoros.
              </p>
              <p>Se, porventura, os meteoros forem grandes e resistentes o suficiente para não se desintegrar e
                atingirem a superfície da Terra, eles serão chamados de meteoritos. Existe a chance de um meteorito
                atingir uma região habitada, mas a maior probabilidade é de que esses corpos caiam nos oceanos, já que
                nosso planeta é em grande parte formado por água. </p>

              <h2>O que é a chuva de meteorito?</h2>
              <p>Quando a Terra, seguindo seu movimento de translação ao redor do Sol, passa por
                uma região onde há o rastro da passagem de um cometa, fragmentos oriundos do
                cometa podem entrar na atmosfera terrestre em grandes quantidades, formando
                inúmeros meteoros. Esse fenômeno é chamado de chuva de meteoros.
                A grande maioria dos corpos celestes é muito pequena para chegar à condição de
                meteorito e atingir a superfície terrestre, mas os danos a satélites em órbita ao redor
                da Terra são possíveis. </p>

              <h2>Como observar um chuva de meteoros?</h2>
              <p>Esse fenômeno pode ser visto a olho nu com a ajuda de um binóculo ou até com o auxílio de uma máquina fotográfica que permita controlar o tempo de exposição de seu filme. O momento em que a Terra intercepta a órbita de cometas pode ser previsto, mas o momento exato da ocorrência das chuvas é uma incógnita. Para observar a chuva com maior nitidez, recomenda-se procurar um local alto e afastado da cidade para que a luz e a poluição não atrapalhem a visualização. A posição “deitado” é a que fornecerá um maior campo visual e conforto. Deve-se evitar o uso de aparelhos que produzam luz, como celulares, pois isso diminui a sensibilidade dos olhos</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default EstrelaCadente
