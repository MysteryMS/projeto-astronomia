import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const andromeda = () => {
  return (
    <main>
      <title>Andrômeda</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItens={'center'}>
              <h1>Galáxia Andrômeda</h1>
              <ArticleAuthor name={'Paola Lavigni Zacariotto'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Formadas por muitas estrelas, poeira cósmica e outros objetos que interagem gravitacionalmente, as Galáxias são agrupamentos desses corpos no universo. A Galáxia de Andrômeda possui formato espiral e sua localização é de 2,54 milhões de anos-luz do Planeta Terra, sua posição é próxima da Constelação de Andrômeda. Segundos pesquisadores e cientistas, é tida como a mais próxima da Via Láctea.</p>
              <p>É assim chamada, de Andrômeda, devido a proximidade da Constelação de Andrômeda, cujo termo é oriundo da Princesa da mitologia Andrômeda, filha de Cefeu (Rei da Etiópia e da Cassiopéia). Sua extensão é a maior de todas as outras galáxias do chamado Grupo Local (composto pela Galáxia do Triângulo, Via Láctea e mais 30 de pequena dimensão). A massa da Galáxia de Andrômeda é praticamente a mesma da nossa, possuindo 7.1x1011 massas solares (massa solar = massa do nosso Sol, o que equivale a 332.946 Terras).</p>
              <p>É um dos astros mais brilhantes e chamativos, com uma magnitude aparente de 3,4, registrado pelo astrônomo francês Charles Messier. Possui ainda de 180 a 220 mil anos-luz de diâmetro. Dentre as principais características, podemos citar que seu corpo celeste é muito estudado e possibilita enormes descobertas científicas, como a estrutura espiral e os conglomerados abertos, a matéria interestelar, o núcleo galáctico, a poeira interestelar entre outras formas impossíveis de serem detectadas na nossa galáxia. </p>
              <p>A Galáxia de Andrômeda foi catalogada como M31 no catálogo Messier, e no NGC 224 (Novo Catálogo Geral), em Outubro do ano de 1786, por John Herschel.</p>
              <p>Com um diâmetro de 250 mil anos-luz, tem o dobro do tamanho da Via Láctea.</p>
              <p>Muitos estudiosos relatam que a Galáxia de Andrômeda terá seu fim próximo, pois com o passar dos anos a Via Láctea e a Galáxia de Andrômeda se aproximam e possivelmente, entrarão em rota de colisão. Esta previsão é prevista para acontecer na mesma época do fim do Sol, em aproximadamente 4 bilhões de anos.</p>
            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default andromeda
