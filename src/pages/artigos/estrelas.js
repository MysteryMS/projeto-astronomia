import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, Link, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const galaxia = () => {
  return (
    <main>
      <title>Estrelas</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItems={'center'}>
              <h1>Estrelas</h1>
              <ArticleAuthor name={'João Gabriel'} image={'/pfps/joao_barbosa.jpeg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>O que são estrelas?</h2>
              <p>As Estrelas são corpos celestes que têm luz própria. Elas são, na verdade, esferas gigantes compostas de gases que produzem reações nucleares mas, graças à gravidade, podem se manter vivas (sem se explodir) por trilhões de anos.</p>
              <p>Na nossa galáxia - a Via Láctea - existem mais de cem bilhões de estrelas. O Sol é uma delas.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>As Cores das Estrelas</h2>
              <p>Existem estrelas vermelhas, amarelas, brancas e azuis. As estrelas emitem luzes de cores diferentes em decorrência da sua temperatura.</p>
              <p>As vermelhas, com cerca de 3000º C, são as que têm a temperatura mais baixa; enquanto com cerca de 40000º C as azuis são as que têm a temperatura mais alta.</p>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>O que é Estrela Cadente?</h2>
              <p>Estrela cadente é o nome popular como é conhecido o <Link underline href="https://www.todamateria.com.br/meteoros/"> meteoro</Link>. A estrela cadente resulta do lançamento de uma partícula sólida que se evapora. O resultado é um efeito luminoso.</p>
              <p>Quando visualizamos um rastro luminoso no céu durante a noite, podemos estar diante do fenômeno da estrela cadente.</p>
              <p>As estrelas cadentes são formadas por fragmentos advindos do espaço interplanetário que se aquecem no momento em que atingem a atmosfera.</p>
            </Grid>


            <Grid xs={12} direction={"column"}>
             <h2>Afinal de Contas, como uma Estrela Nasce?</h2>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/estrelas.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} direction={"column"}>
             <p>Existe uma imensidade de nuvens de gás e poeira espalhadas pelo nosso universo. Chamadas de nebulosas, essas nuvens são formadas principalmente por gás hidrogênio e podem ser consideradas uma espécie de “berçário de estrelas”, já que nelas existe uma grande quantidade de estrelas nascendo ou jovens, como se fossem “estrelas bebês”. Uma nebulosa muito conhecida é a Nebulosa de Órion, próxima às Três Marias e visível a olho nu, se você estiver num local com pouca poluição luminosa.</p>
             <p>Com o colapso, o material no centro do aglomerado começa a se aquecer, formando uma protoestrela; o restante da matéria pode dar origem a outros objetos, mas pode também não formar nada além, continuando a existir como poeira. De pouco a pouco, o que era uma <Link underline href="https://canaltech.com.br/espaco/descoberta-estrela-em-rara-fase-inicial-de-evolucao-e-com-brilho-intenso-177743/"> “estrela bebê”</Link> entra na “adolescência”, com ventos estelares e radiação expelindo a camada de gás e poeira que havia sobrado.</p>
             <p>Quando este envelope é limpo, dizemos que a estrela entrou na fase T Tauri, uma etapa breve de seu processo evolutivo. Mais alguns milhões de anos depois, a temperatura do núcleo da estrela alcançará cerca de 15 milhões de graus Celsius, dando início à fusão dos átomos de hidrogênio em hélio. Neste momento, a estrela entrou para a chamada <Link underline href="https://canaltech.com.br/espaco/quais-estrelas-fornecem-as-melhores-condicoes-para-um-planeta-sustentar-a-vida-177397/">sequência principal</Link>, a fase mais longa de sua vida.</p>
             <p>A fusão libera imensas quantidades de energia em um processo oposto, mas parecido com aquele da detonação de <Link underline href="https://canaltech.com.br/ciencia/o-que-e-uma-explosao-nuclear-181943/">bombas nucleares</Link>. Grande parte das estrelas da Via Láctea está na sequência principal, seguindo tranquilamente com a fusão nuclear em seus interiores. Por exemplo, o Sol tem cerca de 4,6 bilhões de anos e está nesta fase, devendo continuar nela por mais alguns bilhões de anos.</p>
            </Grid>

            <Grid xs={12} direction={"column"}>
             <h2>Estrelas que Nascem em Grupos</h2>
             <p>Os <Link underline href="https://canaltech.com.br/espaco/astronomos-descobrem-aglomerado-estelar-massivo-a-sete-mil-anos-luz-da-terra-186385/"> aglomerados estelares</Link> são formados por grupos de estrelas com uma única origem, que estão gravitacionalmente unidas pelo mesmo período de tempo.</p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/aglomerado-de-estrelas.webp'} showSkeleton={false}  objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} direction={"column"}>
             <p>Há dois tipos de aglomerados estelares: os aglomerados abertos, formados por estrelas que vieram da mesma nuvem molecular; e os aglomerados globulares, que agrupam de milhares a milhões de estrelas em sistemas esféricos.</p>
             <p>Eles são também o lar de algumas das estrelas mais antigas de suas galáxias — tanto que a maioria daqueles presentes nos aglomerados mais massivos se formou durante a "infância" do universo, há cerca de 13 bilhões de anos. Eles mantiveram suas estruturas, mas as estrelas que os formam evoluíram ao longo do tempo. Por isso, elas servem como lembranças físicas das etapas mais remotas da formação estelar.</p>
             <p>Observações do telescópio Hubble mostraram algumas diferenças discretas, mas existentes, nos membros dos <Link underline href="https://canaltech.com.br/espaco/a-via-lactea-esta-engolindo-um-aglomerado-globular-de-estrelas-173342/"> aglomerados globulares</Link>. Há variações químicas e até evidências de que eles guardam gerações múltiplas de estrelas. Devido à intensa atração gravitacional, os aglomerados globulares são mais estáveis, o que permite que sobrevivam durante bilhões de anos.</p>
             <p>Com o <Link underline href="https://canaltech.com.br/espaco/telescopio-hubble-30-anos-de-historia-descobertas-e-revolucao-na-astronomia-163747/"> Hubble</Link>, astrônomos puderam observar aglomerados estelares de diversos tamanhos, usando a espectroscopia (análise e interpretação do espectro eletromagnético dos objetos) para determinar a composição química delas. Graças à precisão das observações, os cientistas vêm usando o telescópio para determinar a luminosidade e temperatura destas estrelas, refinando o conhecimento de como nascem e evoluem.</p>
            </Grid>

            <Grid xs={12} direction={"column"}>
             <h2>Como uma Estrela Morre?</h2>
             <p>De forma geral, quanto maior for a estrela, <Link underline href="https://canaltech.com.br/espaco/como-as-estrelas-morrem-como-funciona-o-ciclo-estelar-173408/"> mais curta será sua vida</Link>, cujo fim chega ao esgotar o hidrogênio em seu núcleo. Quando isso acontece, as reações nucleares param de acontecer; consequentemente, sem o processo de produção de energia que o sustenta, o núcleo da estrela começa a colapsar sobre si, ficando com temperaturas cada vez mais elevadas.</p>
             <p>O que acontece a partir daí depende do quão massiva é a estrela em questão. No caso de estrelas como o Sol, este núcleo cada vez mais quente vai empurrar as camadas externas da estrela para fora, fazendo com que ela se infle feito um balão e se torne uma gigante vermelha. É esse o <Link underline href="https://canaltech.com.br/espaco/o-que-acontecera-com-o-sistema-solar-apos-a-morte-do-sol-196175/"> destino da nossa estrela</Link> em alguns bilhões de anos. Quando isso acontecer, os planetas rochosos do Sistema Solar deverão ser engolidos pelas camadas expandidas do Sol — incluindo a Terra.</p>
            </Grid>
            <Grid md={12} alignItems={'center'} justify={'center'} xs={12} >
              <Image src={'/estrela-morrendo.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={"column"}>
             <p>Nessa etapa de "morte" estelar, existem destinos variados. Se a estrela for "média", como o Sol, continuará expelindo suas camadas até deixar apenas o núcleo exposto, formando uma anã branca, que se esfriará gradualmente. Mas, no caso de estrelas muito mais massivas do que o Sol, o destino não é uma expansão seguida de resfriamento: estrelas com pelo menos oito massas solares explodem em <Link underline href="https://canaltech.com.br/espaco/quem-descobriu-as-supernovas-183950/"> supernovas</Link> quando estão no fim de suas vidas.</p>
             <p>E se o núcleo agonizante no coração da supernova tiver de três a cinco massas solares, o processo de colapso continua e pode ser que isso gere uma <Link underline href="https://canaltech.com.br/espaco/colisao-entre-buraco-negro-e-estrela-de-neutrons-e-confirmada-pela-primeira-vez-188578/"> estrela de nêutrons</Link>, objeto extremamente denso e com imensa força gravitacional. Já caso o núcleo colapsado da estrela tenha mais do que cinco massas solares, ele pode colapsar completamente, dando origem a um <Link underline href="https://canaltech.com.br/espaco/colisao-entre-buraco-negro-e-estrela-de-neutrons-e-confirmada-pela-primeira-vez-188578/">buraco negro.</Link></p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

// TODO: add sources & related content

export default galaxia
