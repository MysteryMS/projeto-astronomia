import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

//TODO: adicionar músicas
const Sol = () => {
  return (
    <main>
      <title>Tudo Sobre o Sol</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} direction={'column'} alignItems={'center'}>
              <h1>Tudo Sobre o Sol</h1>
              <ArticleAuthor name={"Leonardo de Paiva"} image={'/pfps/leonardo.jpeg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Como definir o sol?</h2>
              <p>O Sol é a estrela mais próxima da Terra, dista aproximadamente 150 milhões de quilômetros de nós, e é responsável por manter todo o Sistema Solar em sua interação gravitacional: oito planetas e os demais corpos celestes que o compõem, como planetas anões, asteroides e cometas.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Qual o tipo de estrela do Sol?</h2>
            </Grid>
            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/tipodesois.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>As estrelas de sequência principal, chamadas de estrelas anãs, são a absoluta maioria das estrelas no Universo, o nosso <b>Sol</b>, por exemplo, trata-se de uma anã amarela, uma <b>estrela </b>de sequência principal de “baixa temperatura” quando comparada às estrelas mais quentes, como as anãs azuis.</p>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <h2>Sol</h2>
              <h3>Descrição</h3>
              <p>O <a href="https://pt.wikipedia.org/wiki/Sol">Sol</a> é a estrela central do Sistema Solar. Todos os outros corpos do Sistema Solar, como planetas, planetas capn, asteroides, cometas e poeira, bem como todos os satélites associados a estes corpos, giram ao seu redor. </p>
              <ul>
                <li><b>• Temperatura na superfície:</b> 5.778 K</li>
                <li><b>• Idade:</b> 4,603 × 10^9 anos</li>
                <li><b>• Massa: 1,989 × 10^30 kg</b></li>
                <li><b>• Raio:</b> 696.340 km</li>
                <li><b>• Tipo espectral:</b> G2V</li>
                <li><b>• Luas:</b> 3122 Florence, 90482 Orco</li>
              </ul>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <h2>Qual é a dimensão do Sol?</h2>
            </Grid>
            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/dimensao-do-sol.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>O <b>Sol</b> tem raio aproximado em 696 mil km, enquanto o diâmetro chega a 1,391 milhões de quilômetros, o que representa mais de 8 vezes o diâmetro de Júpiter. Já o volume do <b>Sol</b> é de 1.412 x 10¹⁸, o que parece bastante impressionante.</p>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <h2>Qual é a verdadeira cor do Sol?</h2>
            </Grid>
            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/verdadeira-cor-sol.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>Portanto, se olharmos para o <b>Sol</b> do espaço, os fótons alcançam nosso córtex visual — a parte do cérebro responsável pelo processamento da informação que chega através dos olhos — simultaneamente. E o resultado que vemos é a luz branca. Essa é considerada a "verdadeira" <b>cor</b> do <b>Sol</b>.</p>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <h2>Qual a cor do Sol para a Nasa?</h2>
              <p>Dentro dessa classificação astronômica, o Sol é considerado uma estrela anã amarela.</p>
            </Grid>
            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/cor-do-sol-para-nasa.webp'} showSkeleton={false} objectFit={'cover'}/>
            </Grid>



            <Grid xs={12} direction={'column'}>
            <h2>14 CURIOSIDADES SOBRE O SOL</h2>
            <p>O Sol é nossa grande estrela, que possibilita a vida na Terra e mantém o sistema solar unido. A vida de todos os seres vivos da Terra depende dele, que fornece a luz e o calor para nós. Agora, conheça algumas curiosidades sobre o Astro Rei:</p>

            <ul>
              <li>1- A quantidade de energia liberada pelo Sol em um segundo é superior a produzida pelo ser humano em toda a história.</li>
              <li>2- Sua temperatura ultrapassa 5.500 C. Calcula-se que em seu centro, o Sol tenha uma temperatura de 15 milhões de graus centígrados.</li>
              <li>3- O Sol possui 99,86% de toda a massa do Sistema Solar, e é 300 mil vezes mais pesado do que a Terra.</li>
              <li>4- Uma pessoa de 70 quilos pesaria cerca de 1.900 quilos no Sol.</li>
              <li>5- A luz do Sol demora 8 minutos para chegar na Terra. Isso significa que se o Sol explodisse agora, nós só perceberíamos daqui a 8 minutos.</li>
              <li>6- A cada segundo de brilho, o Sol libera uma quantidade de energia equivalente a 1 milhão de bombas de hidrogênio.</li>
              <li>7- Se o Sol tivesse apenas um centímetro de diâmetro, a estrela mais próxima estaria a 285 quilômetros de distância.</li>
              <li>8- Tanto a aurora austral (que ocorre no polo sul) quanto a aurora boreal (comum no polo norte) são provocadas pelas partículas do vento solar lançadas na atmosfera terrestre.</li>
              <li>9- O Sol leva 250 milhões de anos para dar uma volta completa na Via Láctea.</li>
              <li>10- A distância entre o Sol e a Terra é de 149,45 milhões de quilômetros. Durante o inverno, o Sol fica 4,83 milhões de quilômetros mais perto da Terra.</li>
              <li>11- As sombras dos objetos são maiores no inverno do que no verão.</li>
              <li>12- Às vezes, o Sol, a Terra e a Lua ficam alinhados. Quando a Lua se coloca entre eles, ocultando o Sol, temos um eclipse solar.  Pela velocidade em que o Sol se move, é impossível que um eclipse solar dure mais que 7 minutos e 58 segundos. No eclipse lunar, a Lua entra na sombra da Terra. Na maior parte do tempo, a Lua passa acima ou abaixo da sombra porque a sua órbita tem uma inclinação de 5 graus em relação ao percurso descrito pela Terra em volta do Sol.</li>
              <li>13- O Sol é apenas uma das mais de 200 bilhões de estrelas de nossa galáxia, a Via Láctea. Numa noite de céu aberto e sem lua, podemos contar até 2.500 estrelas a olho nu.</li>
              <li>14- Fica a 30 mil anos-luz do centro de nossa galáxia. Para se ter uma ideia da distância: um ano-luz equivale a 9.460.5000.000.000 de quilômetros.</li>
            </ul>
            <p>E com esse poder todo, o sol é uma forma de energia limpa, que pode ser utilizada para amenizar os gastos com a energia elétrica, e agregar um banho com maior conforto à sua rotina.</p>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <h2>2 pontos interessantes:</h2>
              <ul>
                <li>•	O <b>Sol</b> é uma estrela anã de 4,6 bilhões de anos...</li>
                <li>•	Em decorrência da grande temperatura e pressão em seu núcleo, o Sol é capaz de fundir átomos de Hidrogênio em átomos de Hélio, emitindo energia na forma de ondas eletromagnéticas.</li>
              </ul>
            </Grid>


          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default Sol
