import * as React from "react"
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, Image, NextUIProvider, Row} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'
import ImageWithDesc from "../../components/ImageWithDesc"


const astronomiaEAstrologia = () => {
  return (
    <main>
      <title>Astronomia X Astrologia</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Astronomia X Astrologia</h1>
              <Row align={'center'} justify={'center'}>
                <Avatar.Group>
                  <Avatar squared text={'C'}/>
                  <Avatar squared text={'A'} src={'/pfps/aline.jpeg'}/>
                </Avatar.Group>
                <p style={{fontWeight: 300, marginLeft: '15px', fontSize: '25px'}}>Cauê Vinicius & Aline Mattioli</p>
              </Row>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>O que é astronomia?</h2>
              <p>A astronomia é uma das ciências mais antigas do mundo e busca explicar fenômenos e objetos no Universo.
                A compreensão do céu sempre fascinou os seres humanos, que já observavam o firmamento nos primórdios da
                humanidade.</p>
              <p>Uma das provas de que sempre estivemos interessados nos astros são os diversos monumentos arqueológicos
                destinados a observações astronômicas, como Stonehenge, na Inglaterra.</p>
              <p>As observações no princípio da nossa civilização eram metódicas e conferiam movimentos no céu que se
                repetiam. Elas previam onde um corpo celeste estaria em determinada época do ano e a trajetória dele no
                céu.</p>
              <ImageWithDesc image={'/sistema-solar2.webp'} description={'O sistema solar'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Até então, a astronomia e a astrologia não eram tão distintas. Porém, a invenção do telescópio deu
                início à astronomia como a conhecemos hoje. </p>
              <p>Por ser uma ciência natural, a astronomia engloba diversos campos de estudo, como física, química e o
                movimento de corpos celestes. Tudo isto para auxiliar na compreensão de como o espaço se formou e se
                desenvolveu no que se observa atualmente. </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>O que é astrologia? </h2>
              <p>Para a astrologia, os eventos na Terra e em escala humana estão relacionados com os movimentos de
                corpos celestes no céu. </p>
              <p>É considerada hoje em dia uma pseudociência, incompatível com o método científico. No entanto, até o
                século XV, a astronomia e a astrologia eram indistinguíveis uma da outra. </p>
              <p>Os astrólogos acreditam que o posicionamento dos planetas no céu no momento do nascimento de uma pessoa
                define aspectos de seu caráter e personalidade, e o seu destino. </p>
              <p>Muitos defendem questões científicas nesta influência, como os campos eletromagnéticos, mas os
                cientistas refutam essas afirmações. </p>
              <p>No entanto, a astrologia atualmente é vista por grande parte das pessoas como uma forma de proporcionar
                entendimento sobre características da personalidade humana. </p>
              <p>Com isto, a astrologia como pseudociência se baseia em fatos científicos, mas não pode ser comprovada
                pelo método científico. </p>

              <ImageWithDesc image={'/signosdoszodiacos.webp'}
                             description={'Os 12 signos do zodíaco de acordo com a astrologia '}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Também é vista como superstição, pois a física moderna não pode comprovar a influência entre o
                posicionamento dos planetas e eventos na Terra. Isto impede verificar que questões humanas, de fato,
                resultem de tal interação espacial.</p>
              <h2>A ruptura entre a astrologia e a astronomia </h2>
            </Grid>

            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src="/terra.webp" showSkeleton={false} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Na Grécia Antiga, pensadores pré-socráticos como Anaximandro, Xenófanes, Anaxímenes e Heráclides
                chegaram a especular sobre a natureza das estrelas e dos planetas até então conhecidos. Na época de
                Platão, foi criado o modelo cosmológico geocêntrico (que seria posteriormente aceito por Aristóteles),
                que durou até a época de Ptolomeu, que adicionou informações ao modelo para explicar coisas como o
                movimento retrógrado de Marte no céu. </p>
              <p>Antes disso, os babilônios já estudavam as estrelas com o objetivo de prever a influência que os astros
                supostamente tinham sobre os acontecimentos na Terra. Tal estudo foi feito com tanta maestria que eles
                conseguiam seguir, com precisão, os caminhos dos planetas no céu com o passar dos dias e prever futuros
                eclipses da lua. E o impulsionador para esses estudos não era o conhecimento científico, mas sim a ânsia
                por previsões astrológicas precisas. </p>
              <p>Já na Idade Média, os europeus começaram a traduzir textos científicos árabes para o latim, sendo que
                os primeiros textos que foram traduzidos eram tabelas com os movimentos e posições dos planetas, que
                eram usados para fazer previsões astrológicas. E a Igreja Católica, autoridade intelectual central da
                Europa medieval, acabou redefinindo a astrologia do passado, dizendo que ela não era determinista como
                se acreditava antes (ou seja, não determinava o curso de vida de um indivíduo a partir de seu nascimento
                de maneira imutável), mas sim apenas indicativa (o futuro poderia ser mudado de acordo com a vontade de
                Deus). </p>
              <p>A ciência da astronomia começou a se independer gradualmente da pseudociência da astrologia somente ao
                longo do século XVII, conhecido como "A Idade da Razão", sendo que no século seguinte ambas as áreas já
                eram consideradas disciplinas completamente separadas. Alguns astrólogos profissionais foram os
                primeiros nomes que iniciaram, ainda que sem essa intenção, a separação entre astrologia e astronomia, e
                podemos destacar os nomes de Tycho Brahe e Johannes Kepler nessa missão. Esses astrólogos, por sinal,
                acreditavam genuinamente na influência das estrelas em relação aos acontecimentos humanos, e a reforma
                renascentista da astronomia — que acabaria levando à adoção do heliocentrismo — foi impulsionada por
                astrólogos-astrônomos como Kepler. </p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default astronomiaEAstrologia











