import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const asteroides = () => {
  return (
    <main>
      <title>Asteróides do sistema solar</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Os Maiores Asteróides do Sistema Solar</h1>
              <ArticleAuthor name={'Nicolas Rossini Gill'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Os grandalhões</h2>
              <p>Os dois maiores e mais massivos asteroides observados são Ceres e Vesta, com 940 e 520 quilômetros de diâmetro, respectivamente. Eles são conhecidos também como planetas anões, assim como Plutão, que tem 2.400 km e é o maior entre eles, mas que não foi capturado pelos novos instrumentos na melhor resolução por conta da distância.</p>


              <h2>Mais esféricos </h2>
              <p>O Hígia também está na categoria dos asteroides esféricos e pode ser o menor planeta anão existente no sistema solar, segundo o estudo, com 430 km de diâmetro. O objeto é o quarto maior no cinturão de asteroides, depois de Ceres, Vesta e Pallas.</p>


              <h2>Os alongados</h2>
              <p>Além da classe esférica, tem também os asteroides alongados. O maior exemplo da classe é o Cleópatra, que tem o formato semelhante a um osso de cachorro, com duas extremidades salientes conectadas por um eixo grosso.</p>


              <h2>Asteroides e suas luas </h2>
              <p>Em 2008, astrônomos do Instituto SETI em Mountain View, EUA, e do Laboratoire d’Astrophysique, de Marseille, descobriram que o Cleópatra, que tem apenas 40km de diâmetro, é orbitado por duas luas, AlexHelios e CleoSelene, em homenagem aos filhos da rainha egípcia.</p>


              <h2>Mais densos que diamante</h2>
              <p>O estudo destaca ainda a densidade dos corpos celestes e identifica os asteroides Psique e Calíope entre os mais densos na relação gramas por centímetro cúbico, com 4,4 e 3,9 gramas por centímetro cúbico, respectivamente. As matérias que compõem os asteroides são mais densas que o diamante que tem 3,5 gramas por centímetro cúbico.</p>
            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default asteroides
