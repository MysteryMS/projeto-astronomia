import * as React from "react"
import Navbar from "../../components/Navbar"
import { Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'
import ArticleAuthor from "../../components/ArticleAuthor"

const AuroraBoreal = () => {
  return (
    <main>
      <title>Aurora</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Aurora Boreal</h1>
              <ArticleAuthor image={'/pfps/manu_e_elen.jpeg'} name={'Emanuele Vicente & Elen Marissa'}/>
            </Grid>

            {/*            <Grid md={12} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/planetas.webp'} width='1200' height='400' showSkeleton={false}/>
            </Grid>*/}

            <Grid xs={12} direction={'column'}>
              <h2>O que é, e Como se Forma?</h2>
              <p>A aurora boreal é um fenômeno que acontece em regiões polares a noite, ela acontece por conta dos
                impactos de partículas de vento solar com a alta atmosfera terrestre. Falando de maneira mais técnica,
                “A Aurora Boreal é um fenômeno físico-químico resultado do choque do plasma solar com as camadas
                superiores da atmosfera da Terra. Esse plasma é formado por prótons, elétrons e neutrino. Os prótons
                causam auroras fracas e difusas, Suécia. A melhor forma de encontrá-la é se afastando de grandes cidades
                e fontes de luz. </p>

              <h2>Característica</h2>
              <p>A maior característica da aurora boreal são suas cores que são resultado da velocidade e altitude em
                que o plasma entra em contato com os elementos da atmosfera terrestre. As cores mais vistas no fenômeno
                são: azul, vermelho, amarelo, verde, violeta e laranja.</p>

              <h2>Curiosidades</h2>
              <p>- Você sabia que as Auroras são visíveis do Espaço? Muitos satélites que orbitam a Terra conseguem
                registrar fotos impressionantes das luzes multicoloridas.</p>
              <p>- Câmeras fotográficas com a função de longa exposição, acompanhadas do uso de um tripé, conseguem
                registrar lindas fotos de suas cores com mais facilidade e vivacidade, pela sensibilidade e tempo de
                exposição do sensor à luz.</p>
              <p>- Outros planetas também têm auroras como Júpiter, Saturno, Urano e Netuno.</p>

              <h2>Mitos e Lendas Sobre as Auroras</h2>
              <p>- No estado de Washington, índios Makah achavam que as luzes eram fogueiras no norte criadas por uma
                tribo de anões que o usavam para ferver gordura de baleia. O povo Mandan na Dakota do Norte pensavam
                parecido. Eles achavam que eram fogueiras utilizadas por grandes guerreiros para ferver seus inimigos em
                grandes panelas.</p>
              <p>- No norte da América havia muitas lendas e mitos extremamente diferentes. Haviam lendas em que as luzes
                eram corvos e lendas em que eram tochas seguradas por guias para os que partiam para o próximo mundo.
                Mitos de que eram espíritos daqueles que morreram de forma violenta, espíritos celebrando a ausência do
                sol, espíritos de animais mortos e espíritos de inimigos vingativos que haviam morrido em combate.</p>
              <p>- Os índios Cree acreditavam que a Aurora era parte do ciclo da vida e que eram espíritos dos mortos que
                permaneciam no céu, mas separados dos que amavam. As luzes seriam a tentativa dos espíritos de se
                comunicar com quem eles tinham deixado para trás na Terra.</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default AuroraBoreal
