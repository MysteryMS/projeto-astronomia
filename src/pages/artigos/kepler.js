import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

const Kepler = () => {
  return (
    <main>
      <title>Kepler-186f</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} direction={'column'} alignItems={'center'}>
              <h1>Kepler-186f</h1>
              <ArticleAuthor name={"Fellipe Franco"} image={'/pfps/fellipe.jpeg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Kepler-186f é um exoplaneta que orbita a Kepler-186. Trata-se do primeiro planeta de tamanho semelhante
                ao da Terra, descoberto na zona habitável de uma estrela. É o planeta mais externo descoberto pela sonda
                Kepler da NASA, lançada no ano de 2009, que orbita uma estrela-anã vermelha a 500 anos-luz da Terra na
                constelação de Cisne, chamada Kepler-186. Kepler-186f faz parte de um sistema de cinco planetas, todos
                com quase o tamanho da Terra, que, no entanto, estão perto demais de suas estrelas para possibilitar a
                vida. O Kepler-186f possui um raio de 1,1 vezes o raio terrestre e um período orbital de 129,9
                dias. </p>
            </Grid>

            <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/kepler.webp'} width={700} height={300} showSkeleton={false} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Massa e composição</h2>
              <p>A massa do exoplaneta Kepler-186f é desconhecida, mas seu valor é estimado entre "0,32M se fosse
                composto de água pura/gelo até 3,77 M se o planeta fosse de ferro puro; se ele possuísse uma composição
                semelhante ao da Terra, (cerca de 1/3 ferro e 2/3 rochas de silicato) resultaria em uma massa
                intermediária de 1,44 M". Pouco se sabe sobre sua atmosfera, e se esta tiver composição parecida com a
                da Terra, poderá ser colonizável a humanos. </p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default Kepler
