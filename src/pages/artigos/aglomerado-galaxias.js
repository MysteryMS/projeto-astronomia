import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, Image, NextUIProvider, Row, Spacer} from "@nextui-org/react"
import {theme} from "../../Theme"

const AglomeradoGalaxias = () => {
  return (
    <main>
      <title>Aglomerado de Galáxias</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container>
            <Grid xs={12} direction={'column'} justify={'center'}>
              <h1>Aglomerado de Galáxias</h1>
              <Row align={'center'} justify={'center'}>
                <Avatar.Group>
                  <Avatar squared text={'P'} src={'/pfps/paola.jpeg'}/>
                  <Avatar squared text={'J'} src={'/pfps/julia_oliveira.jpeg'}/>
                  <Avatar squared text={'A'} src={'/pfps/amanda.jpeg'}/>
                </Avatar.Group>
                <Spacer x={1}/>
                <p style={{fontWeight: 300, fontSize: '25px'}}>Paola Lavigni, Julia Oliveira &
                  Amanda Jullye</p>
              </Row>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>A Via Láctea faz parte do Grupo Local, que é formado por ela, mais duas galáxias
                massivas e dezenas de galáxias anãs. Porém, existem regiões no Universo com
                centenas e até milhares de galáxias próximas com comportamentos bastante
                peculiares comparados aos que observamos aqui na nossa vizinhança. Essas regiões
                são chamadas de aglomerados de galáxias e são as maiores estruturas que
                observamos no Universo!</p>
              <p>Mas que comportamentos peculiares? As galáxias dos aglomerados passam por
                processos muito hostis: elas se movimentam mais rapidamente do que as galáxias do
                Grupo Local e, quando elas se aproximam, a violenta interação gravitacional retira de
                cada galáxia as estrelas velhas e o gás (combustível para formar novas estrelas), que
                se depositarão no meio intraglomerado</p>
              <p>Esse meio pode chegar à temperatura de 100.000.000 °C, emitindo bastante raios X.
                As galáxias com gás remanescente frio podem interagir com esse meio quente e
                perder mais gás. Como consequência, nós observamos que a maioria das galáxias de
                aglomerados é avermelhada (com formas elípticas e lenticulares) e tem pouca
                formação estelar recente.</p>
              <p>Galáxias tendem a se agrupar em aglomerados. Aglomerados tendem a se juntar
                formando superaglomerados.</p>
              <p>Aglomerados de galáxias notáveis no Universo relativamente próximo incluem os
                Aglomerados de Virgem, Fornax, Hércules e Coma Berenices. Um agregado muito
                grande de galáxias conhecido como o Grande Atrator, dominado pelo Aglomerado do
                Esquadro, tem massa suficiente para afetar a expansão local do Universo.</p>
              <p>Cientistas identificaram o proto-superaglomerado de galáxias Hyperion, a maior e mais
                massiva estrutura já encontrada em uma distância e tempo tão grandes — a cerca de
                2,3 bilhões de anos após o Big Bang.</p>
              <p>As protogaláxias também são conhecidas como galáxias primitivas. Já o
                superaglomerado de galáxias é um enorme aglomerado de galáxias. Ou seja, Hyperion
                é um grande aglomerado de galáxias primitivas. Ele está localizado no campo
                COSMOS na constelação do Sextante.</p>
              <Image src={'/aglomerado_galaxias.jpeg'} height={300} showSkeleton={false}/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default AglomeradoGalaxias
