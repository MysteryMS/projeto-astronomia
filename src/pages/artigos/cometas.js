import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, Link, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const cometas = () => {
  return (
    <main>
      <title>Cometas</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Cometas</h1>
              <ArticleAuthor name={'Bruno Rodrigues'} image={'/pfps/bruno.jpeg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Os cometas são corpos celestes que, junto com os planetas e asteroides, integram o Sistema Solar. Eles são descritos por astrônomos como "pedras de gelo sujo". Isso se deve a sua constituição, que é, basicamente, gases congelados, poeira cósmica e rochas. </p>
              <p>Nesse ponto, os cometas se diferenciam dos planetas, por exemplo, que possuem órbitas mais regulares, praticamente em formato de círculos. Esses corpos celestes possuem tempo de vida variável. É considerado um tempo de vida curto aquele que é menor a 200 anos, por outro lado, o longo varia de centenas a centenas de milhares de anos. </p>
            </Grid>
            <Grid alignItems={'center'} justify={'center'} xs={12} >
              <Image src={'/cometa.webp'} width={400} showSkeleton={false} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>As estruturas que formam os cometas</h2>
              <p>Os cometas são formados por três partes: cabeleira, cauda e núcleo. A formação dessas estruturas está diretamente ligada com o aquecimento do gelo e dos demais materiais que constituem o corpo celeste. </p>
              <p>A cabeleira ou coma funciona como uma capa para o núcleo. Fazendo uma comparação com a estrutura existente nos planetas, a cabeleira seria uma espécie de <Link underline href="https://www.educamaisbrasil.com.br/enem/astronomia/atmosfera">atmosfera</Link>. Em sua composição são encontrados gases a base de <Link href="https://www.educamaisbrasil.com.br/enem/quimica/hidrogenio" underline>hidrogênio</Link> e <Link href="https://www.educamaisbrasil.com.br/enem/quimica/oxigenio" underline>oxigênio.</Link></p>
              <p>A cauda, por sua vez, é uma espécie de prolongamento da cabeleira. Elas são formadas devida a ação do vento solar, que sopra a coma criando uma espécie de rastro da nuvem. </p>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <h2> Diferenças de cometa ou meteoro</h2>
              <p>Os cometas e os meteoros são fenômenos astronômicos dos quais se possui bastante conhecimento, mas ainda assim é possível que haja confusão para diferenciá-los. Contudo, para isso, alguns aspectos podem ser tomados como referência. São eles: a localização, tamanho, temperatura, dimensão da cauda e duração.</p>
              <p>Como já foi dito, o cometa é um corpo celeste que integra o Sistema Solar, ele é interplanetário. O meteoro, por sua vez, acontece na atmosfera do <Link href="https://www.educamaisbrasil.com.br/enem/geografia/planeta-terra" underline>planeta Terra</Link>. Além disso, enquanto os primeiros possuem milhares de quilômetros; o segundo, na grande maioria, possui centímetros.</p>
              <p>O tempo de visualização de um cometa, enquanto ele passa pelas proximidades do Sol, pode durar dias. Um meteoro, por outro lado, é visto durante alguns segundos. No que diz respeito à temperatura, já sabemos que os primeiros são verdadeiras pedras de gelo, enquanto os meteoros são pedras em chamas.</p>
            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default cometas
