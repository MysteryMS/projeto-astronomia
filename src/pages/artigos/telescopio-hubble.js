import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'
import ArticleAuthor from "../../components/ArticleAuthor"
import ImageWithDesc from "../../components/ImageWithDesc"

const TelescopioHubble = () => {
  return (
    <main>
      <title>Telescópio Escial Hubble</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Telescópio Espacial Hubble</h1>
              <ArticleAuthor name={'João Pedro Pesuto'}/>
            </Grid>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <p>O Telescópio Hubble é um satélite artificial não tripulado, foi lançado pela NASA ao espaço no dia 24
                de abril de 1990, no Discovery (ônibus espacial) durante a missão STS-31. Ele carrega um grande
                telescópio para luz visível e infravermelha. </p>
              <p>Ele foi um grande avanço, pela primeira vez foi possível ver e estudar, mais detalhadamente e
                precisamente estruturas desconhecidas ou pouco conhecidas além da nossa galáxia. A progressão foi tanta
                que poderia ser equivalente a progressão da luneta de Galileu no século XVII, pois permitiu a humanidade
                uma nova visão do universo. O Hubble é a primeira missão da NASA da categoria “Grandes Observatórios
                Espaciais “(Great Observatories Program), a qual contém quatro observadores orbitais que observam o
                universo em luz visível, raios gama, raios-X e infravermelho.</p>
            </Grid>
            <Grid justify={'center'} xs={12}>
              <h2>Falha no Espelho</h2>
            </Grid>
            <Grid justify={'center'} xs={12} md={6}>
              <p>Pouco tempo após o lançamento, o telescópio já apresentava problemas pelas imagens que voltavam, ficou
                evidente que havia problemas no sistema óptico e que o foco esperado não foi atingido pelo telescópio.
                Imagens de fontes pontuais eram difusas em um raio de mais de 1 arcsec, em vez de ter uma função de
                espalhamento pontual (PSF) dentro de um círculo de 0,1 arcsec de diâmetro, como havia sido especificado
                nos requisitos técnicos do projeto.
              </p>
            </Grid>
            <Grid xs={12} md={6} direction={'column'} justify={'center'} alignItems={'center'}>
              <ImageWithDesc image={'/falha-espelho-hubble.webp'}
                             description={'Comparação das imagens antes e depois da correção da falha do espelho'}/>
            </Grid>
            <Grid xs={12} justify={'center'} direction={'column'}>
              <p>Após a análise das imagens foi identificado que o problema era que o espelho principal tinha sido
                construído com uma forma errada, embora que provavelmente era o espelho mais precisamente construído de
                todos os tempos, com variações de apenas 10 nanômetros a partir da curva prevista, era plano em demasia
                nas bordas em cerca de 2 200 nanômetros (2,2 mícrons). Esta pequena diferença foi catastrófica,
                produzindo uma aberração esférica grave. </p>
              <p>A falha do espelho afetou relativamente pouco a observação em alta resolução de objetos brilhantes e a
                espectroscopia, no entanto, a perda de luz no grande halo desfocado em redor reduzia severamente a
                utilidade do telescópio para objetos de brilho fraco ou para imagens de alto contraste.</p>
              <p>Mesmo com os problemas e sem as correções que iriam por vir, o Hubble conseguiu muitas observações
                produtivas, O erro foi bem caracterizado e era estável, permitindo aos astrônomos otimizar os resultados
                obtidos através de técnicas compensatórias sofisticadas de processamento de imagem. </p>
              <p>Para se corrigir o problema da aberração esférica foi estabelecido o sistema Corrective Optics Space
                Telescope Axial Replacement (COSTAR), constituído por dois espelhos de compensação da falha. Para
                ajustar o sistema COSTAR no telescópio, um dos outros instrumentos teve de ser removido, e os astrônomos
                selecionaram o High Speed Photometer para ser sacrificado. Várias missões do ônibus espacial foram
                lançadas para concertos, substituição de instrumentos e outros ajustes</p>
            </Grid>
            <Grid justify={'center'} direction={'column'}>
              <h2>Impacto na Astronomia</h2>
              <ul type={'circle'}>
                <li><p>• O Hubble tem ajudado a resolver problemas da astronomia e revelado novos resultados. Um de seus
                  maiores feitos é a medição das distâncias das cefeidas com precisão inédita e, com isso, limitando o
                  valor da constante de Hubble, a medida da taxa na qual o universo está em expansão, que também está
                  relacionada com a sua idade. Antes do lançamento do Hubble, as estimativas da constante de Hubble
                  tinham erros de até 50%, mas as medições do Hubble de cefeidas no aglomerado de Virgem e outros
                  aglomerados de galáxias distantes forneceu um valor medido com uma precisão de ± 10%, o que é
                  consistente com outras medidas mais precisas feitas desde o lançamento do Hubble usando outras
                  técnicas. </p></li>
                <li><p>• O Hubble ajudou a melhorar as estimativas da idade do universo e colocou dúvidas sobre o seu
                  futuro. Também usaram o telescópio para observar supernovas distantes e descobriram evidências de que,
                  longe de desacelerar sob a influência da gravidade, o universo pode de fato estar se expandindo em uma
                  taxa de aceleração. Esta aceleração foi posteriormente medida com mais precisão por outros telescópios
                  terrestres e espaciais, confirmando a constatação do Hubble. A causa desta aceleração permanece mal
                  compreendida, mas atribui-se mais comumente à influência da energia escura. </p></li>
                <li><p>• Os espectros e imagens de alta resolução fornecidos pelo Hubble têm sido úteis para estabelecer
                  a prevalência de buracos negros no núcleo de galáxias próximas. Embora isso já tivesse sido suposto
                  desde a década de 1960 que os buracos negros seriam encontrados nos centros de algumas galáxias, coube
                  ao Hubble contribuir para mostrar que os buracos negros são, provavelmente, comuns nos centros
                  galácticos, e que as massas dos buracos negros e as propriedades destas galáxias estão intimamente
                  relacionadas. </p></li>
                <li><p>• A colisão do cometa Shoemaker-Levy 9 com Júpiter em 1994 foi um presente para os astrônomos,
                  acontecendo apenas alguns meses após uma missão de manutenção restaurar o desempenho óptico do Hubble.
                  As imagens foram cruciais para o estudo da dinâmica da colisão de um cometa com Júpiter. </p></li>
                <li><p>• Outras grandes descobertas feitas usando seus dados incluem discos proto-planetários na
                  nebulosa de Orion, evidências da presença de planetas extra-solares em torno de estrelas como o Sol e
                  as contrapartidas ópticas das ainda misteriosas explosões de raios gama. O Hubble também tem sido
                  usado para estudar os objetos nos confins do Sistema Solar, incluindo planetas anões Plutão e
                  Eris. </p></li>
                <li><p>• Um legado único do Hubble são as imagens produzidas pelas câmeras de espaço profundo e ultra
                  profundo, de sensibilidade inigualável em comprimentos de onda visíveis, criando imagens das mais
                  distantes regiões do céu já obtidas nestes comprimentos de onda. As imagens revelam galáxias a bilhões
                  de anos-luz de distância, e têm tanto gerado como subsidiado uma grande quantidade de trabalhos
                  científicos, abrindo uma nova janela para o Universo primordial. </p></li>
                <li><p>• Outros fatos mostram o impacto positivo do telescópio na astronomia. Mais de 9 000 trabalhos
                  com base em dados do Hubble foram publicados em jornais peer-reviewed, e inúmeros outros surgiram em
                  canais de eventos. Em média, um trabalho com base em dados do Hubble recebe cerca de duas vezes mais
                  citações do que os com base em dados não-Hubble. Dos 200 artigos científicos publicados a cada ano que
                  recebem mais citações, cerca de 10% são baseadas em dados do Hubble. </p></li>
                <li><p>• Embora o Hubble tenha claramente um impacto significativo na pesquisa astronômica, o custo
                  financeiro desse impacto tem sido grande. Um estudo sobre os impactos relativos à astronomia de
                  tamanhos diferentes de telescópios descobriu que enquanto os trabalhos com base em dados do Hubble
                  geram 15 vezes mais citações do que os baseados em telescópios de terra, os custos de construção e
                  manutenção do Hubble são cerca de 100 vezes maiores. </p></li>
                <li><p>• Hubble melhor que satélites terrestres? Depende da pesquisa a ser feita. A utilidade da óptica
                  adaptativa versus as observações do Hubble, Nas faixas visíveis, a óptica adaptativa só pode corrigir
                  um campo relativamente pequeno de vista, enquanto o Hubble pode produzir imagens ópticas de alta
                  resolução ao longo de um vasto campo. Apenas uma pequena fração de objetos astronômicos pode ser
                  registrada em alta resolução a partir da Terra, em contraste, o Hubble pode realizar observações de
                  alta resolução de qualquer parte do céu noturno, e em objetos extremamente pálidos.</p></li>
              </ul>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}
// TODO: add source (Wikipedia)
export default TelescopioHubble
