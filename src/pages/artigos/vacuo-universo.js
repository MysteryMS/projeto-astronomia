import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, Image, NextUIProvider, Row, Spacer} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'

const VacuoUniverso = () => {
  return (
    <main>
      <title>O Vácuo do Universo</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container>
            <Grid xs={12} direction={'column'}>
              <h1>O Vácuo do Universo</h1>
              <Row align={'center'} justify={'center'}>
                <Avatar.Group>
                  <Avatar squared text={'S'} src={'/pfps/samuel.jpeg'}/>
                  <Avatar squared text={'G'}/>
                  <Avatar squared text={'D'}/>
                </Avatar.Group>
                <Spacer x={1}/>
                <p style={{fontWeight: 300, fontSize: '25px'}}>Samuel Reis, Gustavo Telini & Diogo Henrique</p>
              </Row>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>O vácuo é, em teoria, a ausência total de matéria em uma certa região. Pena que, na prática, é
                impossível tirar toda a matéria de qualquer lugar — o espaço que há entre os planetas, estrelas e
                satélites, claro, é vazio o bastante para os padrões humanos.</p>
              <p>Já faz algum tempo, porém, que a física pensa nas propriedades do vazio. Na década de 1930, dois
                célebres físicos fizeram previsões teóricas sobre um fenômeno chamado birrefringência do vácuo. Um
                deles, velho conhecido dos fãs de Breaking Bad, é Werner Heisenberg, pai da mecânica quântica. O outro é
                Hans Heinrich Euler, orientando de doutorado de Heisenberg que você não deve confundir com o Euler da
                matemática. Agora, mais de 80 anos depois, um grupo de cientistas liderado pelo italiano Roberto
                Mignani, do Istituto di Astrofisica Spaziale e Fisica Cosmica Milano, conseguiu observar o fenômeno na
                prática pela primeira vez.</p>
              <p>A observação veio "por tabela" quando o grupo de astrônomos, que também tem membros da Polônia e da
                Inglaterra, estudava a estrela de nêutrons RX J1856.5-3754, a 400 anos-luz da Terra, usando o Very Large
                Telescope (VLT) do Observatório Europeu do Sul (ESO), no Chile.</p>
              <p>Uma estrela de nêutrons também conhecida como "pulsar" é o que resta do núcleo de uma estrela muito,
                muito com grande (com massa mais de oito vezes maior que a do Sol) depois que ela ejeta boa parte de sua
                matéria no espaço em uma explosão espetacular chamada "supernova".</p>
              <p>Além de extremamente densa, ela também tem um campo magnético fortíssimo. A birrefringência do vácuo
                ocorre justamente quando uma área do vácuo faz com que a luz se polarize, sob influência de um campo
                magnético tão forte quanto o de um pulsar, este efeito age como um prisma sobre a luz.</p>
              <p>"Esse efeito só poderia ser detectado na presença um campo magnético muito forte, como o de uma estrela
                de nêutrons. Isso prova, mais uma vez, que estrelas de nêutrons são laboratórios valiosos para estudar
                as leis fundamentais da natureza".</p>
              <Image src={'/vacuo_universo.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Físicos Apresentados</h2>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Werner Heisenberg</h3>
              <p>Foi um famoso físico ganhador de Prêmio Nobel. Iniciou o curso de Física em 1920, em Munique. Durante
                um Congresso em Copenhague, de Niels Bohr, Heisenberg expôs suas ideias sobre a Mecânica Quântica e a
                partir daí, ficou muito amigo de Bohr.</p>
              <Image src={'/werner_heisenberg.webp'} showSkeleton={false}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <h3>Hans Heinrich Euler</h3>
              <p>Foi um físico alemão. Doutorado em 1935 na Universidade de Leipzig, orientado por Werner Heisenberg, com a tese Über die Streuung von Licht an Licht nach der Diracschen Theorie.</p>
              <Image src={'/hans_euler.webp'} showSkeleton={false}/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

//TODO: add sources
export default VacuoUniverso
