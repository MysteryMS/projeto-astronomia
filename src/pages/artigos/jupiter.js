import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, NextUIProvider, Row, Spacer} from "@nextui-org/react"
import {theme} from "../../Theme"

const Jupiter = () => {
  return (
    <main>
      <title>Júpiter</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Júpiter</h1>
              <Row align={'center'} justify={'center'}>
                <Avatar.Group>
                  <Avatar squared text={'O'} src={'/pfps/otavio.webp'}/>
                  <Avatar squared text={'T'}/>
                </Avatar.Group>
                <Spacer x={1}/>
                <p style={{fontWeight: 300, fontSize: '25px'}}>Otávio Goulart & Tiago Costa</p>
              </Row>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Júpiter é o quinto planeta a partir do Sol e o maior do Sistema Solar . É um gigante gasoso com uma
                massa mais de duas vezes e meia a massa de todos os outros planetas do Sistema Solar combinados, mas um
                pouco menos de um milésimo da massa do Sol. Júpiter é o terceiro objeto natural mais brilhante no céu
                noturno da Terra depois da Lua e Vênus , e as pessoas o observam desde os tempos pré -históricos . Foi
                nomeado após o deus romano Júpiter o rei dos deuses.</p>
              <p>A atmosfera externa é visivelmente segregada em várias bandas em diferentes latitudes, com turbulência
                e tempestades ao longo de seus limites de interação. Um resultado proeminente disso é a Grande Mancha
                Vermelha, uma tempestade gigante conhecida por existir desde pelo menos o século 17, quando os
                telescópios a viram pela primeira vez, Júpiter tem 80 luas conhecidas e possivelmente muitas mais.</p>
              <ul>
                <li><p>• Se morássemos em Júpiter, faríamos aniversário de 12 em 12 anos, tempo de translação do
                  planeta.</p></li>
                <li><p>• O dia em Júpiter tem 10 horas de duração.</p></li>
                <li><p>• Europa, uma das Luas Galileanas, pode ter um oceano líquido em sua superfície.</p></li>
                <li><p>• Ventos de 600 km/hora são comuns em Júpiter.</p></li>
                <li><p>• A massa de Júpiter é 2,5 vezes maior do que os outros sete planetas do Sistema Solar juntos. Um
                  gigante.</p></li>
                <li><p>• Caso o interior de Júpiter fosse oco, caberiam 1300 Terras dentro dele.</p></li>
                <li><p>• É um planeta gasoso, como Saturno, Urano e Netuno.</p></li>
                <li><p>• Júpiter possui a maior velocidade de rotação entre os planetas do Sistema Solar.</p></li>
                <li><p>• A força gravitacional em Júpiter é de 22,9 m/s², enquanto na Terra essa força é de 9,8
                  m/s².</p>
                </li>
              </ul>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default Jupiter
