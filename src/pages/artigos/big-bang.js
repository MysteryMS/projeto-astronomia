import * as React from "react"
import Navbar from "../../components/Navbar"
import ArticleAuthor from "../../components/ArticleAuthor"
import "../../index.css"
import {Container, Grid, NextUIProvider, Text} from "@nextui-org/react"
import {theme} from "../../Theme"
import BigBangCard from "../../components/BigBangCard"

const BigBang = () => {
  return (
    <main>
      <title>Como Surgiu o Big Bang</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={`center`} direction={'column'}>
              <h1>Como Surgiu o Big Bang</h1>
              <ArticleAuthor name={'Helano Rangel'}/>
            </Grid>

            <Grid xs={12}>
              <p>As principais equações foram feitas por Alexander Friedmann,  dois anos depois viria a ser comprovada por Edwin Hubble e, décadas depois, George Gamow aprofundaria a teoria.</p>
            </Grid>
            <Grid xs={12}>
              <p style={{textAlign: 'justify'}}>
              Em 1929, Hubble descobriu  que as distâncias de galáxias distantes eram geralmente proporcionais aos seus desvios para o vermelho, como sugerido por Lemaître em 1927. Esta observação foi feita para indicar que todas as galáxias e aglomerado de galáxias muito distantes têm uma velocidade aparente diretamente fora do nosso ponto de vista: quanto mais distante, maior a velocidade aparente. Sabendo disso, imagine o universo como um vídeo, no momento atual as galáxias estão se afastando umas das outras, sendo assim, se voltarmos o vídeo as veremos cada vez mais próximas e, quando chegasse no início dele, toda a matéria do universo estaria concentrada em um único ponto.
              </p>
            </Grid>
            <Grid xs={12}>
              <p>Após a descoberta da radiação cósmica de fundo em micro-ondas em 1964, e especialmente quando seu espectro (ou seja, a quantidade de radiação medida em cada comprimento de onda) traçou uma curva de corpo negro, muitos cientistas ficaram razoavelmente convencidos pelas evidências de que alguns dos cenários propostos pela teoria do Big Bang devem ter ocorrido. A importância da descoberta da radiação cósmica de fundo é que ela representa um “fóssil” de uma época em que o universo era muito novo, sendo a maior evidência do Big Bang. Após a descoberta da radiação cósmica de fundo em micro-ondas em 1964, e especialmente quando seu espectro (ou seja, a quantidade de radiação medida em cada comprimento de onda) traçou uma curva de corpo negro, muitos cientistas ficaram razoavelmente convencidos pelas evidências de que alguns dos cenários propostos pela teoria do Big Bang devem ter ocorrido. A importância da descoberta da radiação cósmica de fundo é que ela representa um “fóssil” de uma época em que o universo era muito novo, sendo a maior evidência do Big Bang.</p>
            </Grid>
            <Grid xs={12}>
              <h2>A Cronologia do Big Bang</h2>
            </Grid>
            <Grid xs={12}>
              <p><b>Início, 0s –</b> Algo iniciou o Big Bang, mas nós não temos como saber.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>Era de Planck, 10⁻⁴³s após o princípio –</b> Nesse tempo, toda a matéria e energia está comprimida em um espaço 100 bilhões de bilhões de vezes menor do que o núcleo atômico. Nesse estágio extremo do umiverso, nosso conhecimento não possibilita sequer imaginar como isso é possível, ou se era realmente assim, portanto, assim como tudo na ciência, é possível que esse entendimento mude daqui um tempo.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>Era da Grande Unificação, 10⁻³⁶s após o princípio –</b> Nesse tempo, a temperatura do universo era de 100.000.000.000.000.000.000.000.000.000.000.000º Celsius. O senso comum diria que isso cegaria qualquer um instantaneamente, mas nesse momento o universo possuia sua matéria tão compactada que a luz não conseguia passar por ela. O universo era escuro, a luz “não existia”.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>Inflação, 10⁻³²s após o princípio –</b> Nesse tempo, o universo começou a se expandir absurdamente rápido, aumentando seu tamanho em 100 trilhões de trilhões de vezes. Esse momento é o que explica o porquê do universo não ter se transformado em um buraco negro. Além disso, esse tempo nos mostra que o Big Bang não foi uma explosão, mas uma inflação.</p>
            </Grid>
            <Grid xs={12}>
              <p>Após a inflação o universo esfria um pouco, 10 milhões de vezes, mas ainda assim extremamente quente. Porém aqui já conseguimos utilizar as teorias quânticas modernas. Prótons e nêutrons, as partículas que constituem os núcleos atômicos são formadas por Quarks, partículas fundamentais que se mantém unidas através da força nuclear forte, essa força se expressa pela troca de gluóns, partículas fundamentais que carregam a força forte.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>Era dos Quarks e Gluóns, 10⁻¹²s após o princípio –</b> Nesse tempo o universo era muito quente para prótons e nêutrons se formarem. Os quarks e os gluóns se moviam livremente e com muita energia e por isso eles não conseguiam se juntar e formar as primeiras partículas. Nesse momento, alguma coisa desconhecida aconteceu que gerou mais matéria do que anti-matéria.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>Era dos Hádrons, 10⁻⁶s após o princípio –</b> Agora o universo está frio o suficiente e as primeiras partículas começam a se formar. Porém ainda é muito quente para os elétrons orbitarem os átomos, ainda estamos a 10 bilhões de graus Celsius.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>1s após o princípio –</b> Nesse ponto o universo possui 10 anos luz de comprimento, a inflação permite a expansão em uma velocidade mais rápida do que a luz. Foi só nesse momento que os neutrinos conseguem se libertar do gás de partículas do universo primordial.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>10s após o princípio –</b> Nesse ponto o universo é composto principalmente de radiação, elétrons e pósitrons estão se aniquilando.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>3 a 20 min após o princípio –</b> Nesse estágio o universo esfriou o suficiente para a formação dos primeiros núcleos de hélio e lítio.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>50 mil anos após o princípio –</b> A matéria desacelera a expansão do universo. Nesse ponto os primeiros desequilíbrios gravitacionais estão se formando, anteriormente a matéria era distribuída homogeneamente, provavelmente as flutuações quânticas causaram essa concentração de matéria, que permitiu a formação das estrelas e galáxias.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>380 mil anos após o princípio –</b> Os primeiros átomos se forma graças a temperatura do universo. Após o fim desse período ele é composto por 75% de hidrogênio, 24,999…% de hélio e por uma pequena quantidade de lítio.</p>
            </Grid>
            <Grid xs={12}>
              <p>Após a formação dos átomos, o imenso gás que permeia o Cosmos resfria, e com isso a densidade diminui, finalmente permitindo que os fótons escapem, dando origem aos primeiros raios de luz em um universo extremamente escuro. Esses raios são o que chamamos de Radiação Cósmica de Fundo, 1% dos chiados em TVs são causados por isso. Eventualmente com o universo se esfriando ele chegaria a uma temperatura por volta dos 30ºC, permitindo água líquida na superfície dos planetas, causando algumas reações químico biológicas, estamos falando de vida, mas é apenas especulação, essas formas de vida provavelmente não teriam durado até o final dessa era do universo.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>100 milhões de anos após o princípio –</b> O universo esfria ainda mais e a matéria começa a se acumular formando futuras estrelas e buracos negros primordiais, que acreditamos ser os núcleos da maioria das galáxias.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>200 milhões de anos após o princípio –</b> A estrela mais velha já observada começa a brilhar.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>400 milhões de anos após o princípio –</b> A galáxia mais velha já observada é formada.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>1 bilhão de anos após o princípio –</b> A maior parte das galáxias já se formou, inclusive a nossa.</p>
            </Grid>
            <Grid xs={12}>
              <p>Nesse ponto o universo já se expandia bem lentamente devido a atração gravitacional da matéria escura e das galáxias. Mas isso muda 8,8 bilhões de anos após o princípio, por algum motivo o universo começa a se expandir rapidamente de novo, acredita-se que isso tenha ocorrido graças ao que chamamos de energia escura.</p>
            </Grid>
            <Grid xs={12}>
              <p><b>9,2 bilhões de anos após o princípio –</b> Uma supernova espalha a matéria que provavelmente deu início a nossa história, o nosso Sistema Estelar começa a se foemar.</p>
            </Grid>
            <Grid xs={12}>
              <p>Pouco tempo depois, o Sol começa a se formar, nos primeiros 100 mil anos de vida sua órbita era composta por restos de matéria que começam a se chocar depois de um tempo, formando os planetas gasosos Júpiter, Saturno, Urano e Netuno. Nesse período o espaço entre o Sol e Júpiter é muito caótico e a formação dos planetas rochosos só terminou 3 milhões de anos depois. Mas a partir desse ponto, o Sistema Solar é parecido com como ele é hoje.</p>
            </Grid>
            <Grid>
              <p>Nós não sabemos de onde o universo surgiu, mas o Big Bang nos dá uma excelente visão do que aconteceu após isso. Nós não possuímos a respota para tudo e talvez nunca teremos a resposta para essa questão e, aceitar isso, é frustrante, no entanto admtir a própria ignorância é o primeiro e mais fundamental passo para obtermos conhecimento sobre tudo aquilo que nos rodeia.</p>
            </Grid>
            <Grid>
              <p>“Admitir a ignorância, é abrir a porta para a sabedoria” – Sócrates </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <Text h2>Conteúdo Relacionado</Text>
            </Grid>
            <Grid direction={'column'}>
              <BigBangCard/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default BigBang
