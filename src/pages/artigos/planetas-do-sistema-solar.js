import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import styles from '../../article.css'


const SistemaSolar = () => {
  return (
    <main style={styles}>
      <title>Planetas do Sistema Solar</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'} alignItems={'center'}>
              <h1>Planetas do Sistema Solar</h1>
              <ArticleAuthor name={'Murilo Cruz'} image={'/pfps/murilo.jpeg'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>O nosso sistema solar é formado por 8 planetas, aqui iremos conhecer melhor cada um deles:</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Mercúrio</h2>
              <p>É um planeta bem semelhante com a Lua, não muito grande e repleta de crateras em sua superfície. Lá, a
                vida que conhecemos hoje é simplesmente impossível de existir em Mercúrio, pela sua grande variação
                térmica. Causada pela inexistência de uma atmosfera nesse planeta. Ele é o menor planeta do sistema
                Solar e o mais próximo do Sol.</p>
            </Grid>
            <Grid md={6} xs={12}>
              <Image src={'/mercury.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} md={6} direction={'column'} alignItens={'center'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 58 dias</p>
              <p><b>Tempo de Translação:</b> 87 dias</p>
              <p><b>Variação Térmica:</b> 430°C quando está virado para o Sol e -170°C no lado escuro.</p>
              <p><b>Distância do Sol:</b> 57.910.000 km</p>
              <p><b>Diâmetro Equatorial:</b> 4.879 km</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Vênus</h2>
              <p>Por ser rodeada por uma grande e pesada nuvem formada de dióxido de carbono, a atmosfera de Vênus se
                torna extremamente quente. Ele faz parte da família dos Planetas Terrestre, que são os planetas que
                possuem rochas. Segundo cientistas, Vênus possui montanhas vulcões e duas grandes planícies </p>
            </Grid>
            <Grid md={6} xs={12} justify={'center'} alignItens={'center'}>
              <Image src={'/venus.webp'} showSkeleton={false} height={350} width={350} objectFit={'cover'}/>
            </Grid>
            <Grid md={6} xs={12} direction={'column'} justify={'center'} alignItens={'center'}>
              <p><b>Tempo de Rotação:</b> 243 dias</p>
              <p><b>Tempo de Translação:</b> 224 dias</p>
              <p><b>Variação Térmica:</b> 450°C</p>
              <p><b>Distância do Sol:</b> 108.200.000 km</p>
              <p><b>Diâmetro Equatorial:</b> 12.123 km</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Terra</h2>
              <p>É o único planeta que possui uma camada de gás na atmosfera, o que possibilita a existência de vida
                nele. Por mais que seja chamado de planeta terra, ele é composto de 70% de água. O formato do nosso
                planeta não é completamente redondo, e sim geóide, parecido com uma bolinha de papel amassada. </p>
            </Grid>
            <Grid md={6} xs={12} alignItens={'center'} justify={'center'}>
              <Image src={'/earth.webp'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid alignItens={'center'} xs={12} md={6} justify={'center'} direction={'column'}>
              <p><b>Tempo de Rotação:</b> 24 horas ou 1 dia</p>
              <p><b>Tempo de Translação:</b> 365 dias</p>
              <p><b>Temperatura Média:</b> 15°C</p>
              <p><b>Distância do Sol:</b> 149.600.000 km </p>
              <p><b>Diâmetro Equatorial:</b> 12.756 km</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Marte</h2>
              <p>Chamado de “O Planeta Vermelho”, Marte é o nosso planeta vizinho e apelidado pelos cientistas de “nosso
                futuro lar”, pela sua semelhança em vários aspectos com a nossa Terra, seja pela existência de uma
                crosta terrestre ou até mesmo pela existência de água em seu subsolo. Marte é o segundo menor planeta do
                Sistema Solar, ficando atrás somente de Mercúrio. </p>
            </Grid>
            <Grid md={6} xs={12} justify={'center'}>
              <Image src={'/mars.jpg'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid md={6} xs={12} direction={'column'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 24 horas e 37 minutos</p>
              <p><b>Tempo de Translação:</b> 687 dias</p>
              <p><b>Variação Térmica:</b> 125°C no inverno e 22°C no verão</p>
              <p><b>Distância do Sol:</b> 227.600.000 km </p>
              <p><b>Diâmetro Equatorial:</b> 6.794 km</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Júpiter</h2>
              <p>É um dos Planetas Gasosos do nosso Sistema Solar, são os planetas que são formados por gases e
                elementos leves. Júpiter é de longe o maior planeta do nosso Sistema Solar, sendo 11x maior que a Terra.
                A sua altíssima temperatura e sua pressão esmagadora, faz com que seja impossível de se ter vida nesse
                planeta. </p>
            </Grid>
            <Grid md={6} xs={12}>
              <Image src={'/jupiter.webp'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid xs={12} md={6} direction={'column'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 9 horas e 56 minutos</p>
              <p><b>Tempo de Translação:</b> 11 anos</p>
              <p><b>Variação Térmica:</b> 110°C em sua superfície e 20.000°C em seu núcleo </p>
              <p><b>Distância do Sol:</b> 778.330.000 km</p>
              <p><b>Diâmetro Equatorial:</b> 142.984 km</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Saturno</h2>
              <p>É o segundo maior planeta do Sistema Solar. Ele é muito conhecido pelos seus grande e lindos anéis.
                Saturno também é o segundo Gigante Gasoso do nosso Sistema Solar. Saturno, também é considerado como o
                planeta com maior número de satélites naturais, ele possui mais de 20 satélites identificados até
                hoje. </p>
            </Grid>
            <Grid md={6} xs={12} justify={'center'}>
              <Image src={'/saturn.webp'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid md={6} xs={12} direction={'column'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 10 horas e 42 minutos</p>
              <p><b>Tempo de Translação:</b> 29 anos</p>
              <p><b>Variação Térmica:</b> 130°C em sua superfície e 9.000°C em seu núcleo</p>
              <p><b>Distância do Sol:</b> 1.429.400.000 km ou 1,429 x 109</p>
              <p><b>Diâmetro Equatorial:</b> 120.536 km
              </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Urano</h2>
              <p>Urano é o terceiro maior planeta do Sistema Solar, e assim como Saturno, possui anéis e muitos
                satélites naturais. Em sua composição, ele possui uma mistura densa, com a presença de diferentes tipos
                de gelos e gases, tudo isso em volta de um núcleo sólido, por isso ele é um dos Gigantes Gelados do
                nosso Sistema Solar. A sua cor azul esverdeada, é resultado de muitos gases Metano.</p>
            </Grid>
            <Grid md={6} xs={12}>
              <Image src={'/uranus.webp'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid md={6} xs={12} direction={'column'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 17 horas e 14 minutos</p>
              <p><b>Tempo de Translação:</b> 84 anos</p>
              <p><b>Variação Térmica:</b> -195°C</p>
              <p><b>Distância do Sol:</b> 2.870.000.000 km ou 2,870 x 109</p>
              <p><b>Diâmetro Equatorial:</b> 51.118 km
              </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Netuno</h2>
              <p>Netuno é também considerado um dos Gigantes Gelados do nosso Sistema Solar e é o planeta mais distante
                do Sol. Netuno possui 14 satélites, e os ventos em sua atmosfera é um dos mais rápidos já registrados
                até hoje, podem alcançar até 2.000km/h. Essas fortes ventanias causam imensas tempestades, que são as
                suas manchas azuladas facilmente visíveis em sua atmosfera.</p>
            </Grid>
            <Grid md={6} xs={12} justify={'center'}>
              <Image src={'/netuno.webp'} showSkeleton={false} width={350} height={350} objectFit={'cover'}/>
            </Grid>
            <Grid md={6} xs={12} direction={'column'} justify={'center'}>
              <p><b>Tempo de Rotação:</b> 6 horas e 6 minutos</p>
              <p><b>Tempo de Translação:</b> 165 anos</p>
              <p><b>Temperatura Média:</b> -245°C</p>
              <p><b>Distância do Sol:</b> 4.495.000.000 km ou 4,495 x 109 km </p>
              <p><b>Diâmetro Equatorial:</b> 49.538 km</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default SistemaSolar
