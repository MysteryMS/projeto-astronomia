import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const influenciaDaAstronomia = () => {
  return (
    <main>
      <title>Influência da Astronomia no Egito </title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Influência da Astronomia no Egito</h1>
              <ArticleAuthor name={'Sofia Loren de Moraes Parreira'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
                <h2>Qual era a importância da astronomia para os egípcios? </h2>
                <p>Os egípcios voltavam o estudo da Astronomia para a praticidade com o intuito de predizerem os fatos de importância para eles como, por exemplo, as enchentes do Rio Nilo, bem como para a construção das Pirâmides, orientadas tanto para os pontos cardeais como para as constelações, principalmente de Órion. </p>

                <h2>Como era a Astronomia no Egito antigo? </h2>
                <p>A astronomia egípcia estava muito ligada com a economia”. Valdecir ressalta que a economia estava condicionada às enchentes do Rio Nilo. Portanto, a astronomia inicial no Egito era ligada especialmente ao sol e as influências com os ventos e as chuvas. </p>

                <h2>Deuses do Egito interpretados em Astros </h2>
                <h3>Céu: </h3>
                <p>Na cidade de Heliópolis, a criação é protagonizada pela deusa do céu, Nut, uma primordial (que antecede outras gerações). De acordo com esse mito, Nut é filha de Shu, deus do ar seco, com Téfnis, deusa da umidade, e seria irmã e esposa de Geb, o deus da terra. </p>
                <p>Alguns estudiosos sustentam a ideia de que a deusa Nut simbolizava a faixa da Via Láctea. Normalmente, Nut é representada sob forma humana, de perfil e nua, como se arqueasse seu corpo sobre o deus da terra, tocando o horizonte com os braços e pernas estendidos. Em outras ocasiões, a deusa é representada apoiada em seu pai, Shu. Ela também foi representada de frente no interior de sarcófagos, geralmente engolindo ou dando à luz o sol. </p>

                <h3>Sol:</h3>
                <p>Rá é conhecido como o deus do sol e uma das mais importantes divindades egípcias antigas. Ele também foi associado à construção de pirâmides e à ressurreição dos faraós. Esse deus simbolicamente nascia todas as manhãs com o nascer do sol, e morria com cada pôr do Sol, iniciando sua jornada para o submundo. Assim como no caso de Nut, o principal centro de seu culto era a cidade de Heliópolis, onde, na verdade, era identificado como Atum ou como Atum-Ra. Segundo o mito da cidade em questão, Atum seria o avô de Nut, pai de Shu e Téfnis.</p>
                <p>Outra divindade egípcia relacionada com o Sol é Bastet, filha de Rá. Basicamente, em contrapartida ao mito de Apófis, era ela que tinha o poder sobre os eclipses solares. Era representada como uma mulher com cabeça de gato, e seu centro de culto estava na cidade de Bubástis, na região oriental do Delta do Nilo. Bastet também era tida como uma deusa da fertilidade, além de protetora das mulheres grávidas. Já a guerreira Sekhmet, relacionada com a vingança, também é uma filha de Rá, e por isso é identificava como uma deusa solar. </p>

                <h3>Lua: </h3>
                <p>Por sua vez, a Lua também tinha a sua representação na mitologia egípcia. No caso, uma delas era Iá, que era representado com um disco solar e uma Lua crescente sobre sua cabeça. Segundo o mito, ele poderia também se manifestar como um pássaro ou falcão.O deus Quespisiquis também era representado nesse papel, exceto que representa também a passagem no tempo, e está mais associado à movimentação da Lua. Alguns determinados mitos egípcios atribuem a Quespisquis um fundamental na criação de nova vida em todos os seres vivos. </p>

                <h3>Constelações: </h3>
                <p>Assim como a mitologia grega, que trazia justificativas para as constelações, a egípcia também faz. É o caso, por exemplo, de Sopdet, considerada por muitos estudiosos na área como Sirius. O significado de seu nome, em egípcio, é justamente uma referência ao brilho de Sirius. Sopdet costuma ser representada como uma mulher com uma estrela de cinco pontas sobre a cabeça. Sopdet também foi considerada como uma deusa do solo fértil, responsável por anunciar as inundações do Nilo, protetora da agricultura, do tempo. Vale apontar que Sopdet é a esposa de Sah, uma representação da constelação de Órion. Sah, eventualmente, é identificado como uma das formas de Hórus. </p>

            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default influenciaDaAstronomia
