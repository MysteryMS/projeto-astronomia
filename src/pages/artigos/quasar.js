import * as React from "react"
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'

const quasar = () => {
  return (<main>
      <title>Quasar</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Quasar</h1>
              <Grid xs={12}>
                <Avatar.Group>
                  <Avatar squared text={'F'} src={'/pfps/felipe_godoy.jpeg'}/>
                  <Avatar squared text={'V'} src={'/pfps/victor_brait.jpeg'}/>
                </Avatar.Group>
                <p style={{fontWeight: 300, marginLeft: '15px', fontSize: '25px'}}>Felipe Ricardo & Victor Brait</p>
              </Grid>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Um quasar, abreviação de quasi-stellar radio source (fonte de rádio quase estelar) ou quasi-stellar
                object (objeto quase estelar), é um núcleo galáctico ativo, seu tamanho é maior que uma estrela, porém,
                menor que o mínimo para ser considerado uma galáxia. </p>
              <p>É constituído por um buraco negro supermassivo, com sua massa variando de milhões a bilhões de massas
                solares, cercado por um disco de acreção. O gás existente no disco é acelerado e eventualmente engolido
                pela interação gravitacional com o buraco negro, o que faz com que atinja uma alta velocidade e alta
                temperatura. </p>
              <p>O quasar emite uma quantidade monstruosa de energia na forma de ondas eletromagnéticas, como ondas de
                rádio e luz visível. Os quasares, na verdade, são os maiores emissores de energia do Universo. Um único
                quasar pode atingir luminosidades milhares de vezes maiores que a de uma galáxia como a Via Láctea, por
                exemplo.</p>
              <p>Os primeiros quasares puderam ser identificados na década de 1950, onde eram fontes de rádio de origem
                desconhecida. Em suas primeiras imagens, os quasares apresentavam pontos fracos de luz como o de uma
                estrela, daí surgiu o nome quasar, ou seja, fonte de rádio quase estelar. A luz emitida pelos quasares
                possui altos desvios para o vermelho, isso indica que muitos quasares estão a uma distância muito grande
                da Terra. </p>
              <p>Astrônomos mostraram que os quasares eram mais comuns em um passado distante do nosso Universo e que o
                pico de quasares teria sido há cerca de 10.000 milhões de anos. </p>


              <h2>Exemplo de um quasar</h2>
              <p>O quasar APM 08279+5255 representa o local com maior concentração de água no Universo, este quasar está
                localizado a 12 bilhões de anos-luz da Terra em uma constelação chamada Lynx e tem um volume de 140
                trilhões de vezes o dos oceanos do nosso planeta. Esta quantidade toda de água está acumulada em estado
                gasoso em torno do quasar. </p>
              <p>Com toda essa quantidade de água e moléculas de outros tipos, mostram que há gás suficiente para que o
                buraco negro atinja até 6 vezes o tamanho que se encontra atualmente. O gás que o buraco negro emite
                está em uma temperatura de – 53°C, que representa um calor cinco vezes maior que os gases emitidos por
                nossa galáxia. </p>
            </Grid>

          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>)
}


export default quasar
