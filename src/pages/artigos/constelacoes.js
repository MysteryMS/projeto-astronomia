import * as React from "react"
import Navbar from "../../components/Navbar"
import {Avatar, Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import '../../article.css'

const Constelacoes = () => {
  return (
    <main>
      <title>Constelações</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} alignItems={'center'} direction={'column'}>
              <h1>Constelações</h1>
              <Grid xs={12}>
                <Avatar.Group>
                  <Avatar squared text={'G'} src={'/pfps/gustavo_brandao.jpeg'}/>
                  <Avatar squared text={'J'}/>
                </Avatar.Group>
                <p style={{fontWeight: 300, marginLeft: '15px', fontSize: '25px'}}>Gustavo Brandão & Julia Ziola</p>
              </Grid>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>Constelações são agrupamentos de estrelas ligadas por linhas imaginárias usadas para representar
                objetos, animais, criaturas mitológicas ou deuses. O conceito de constelação surgiu durante a
                Pré-História, quando as pessoas as usavam para descrever suas crenças ou mitologia, por isso, diferentes
                civilizações adotaram, ao longo da história, as suas próprias constelações.</p>
              <p>As constelações também desempenharam importante papel durante as navegações, pois eram usadas como
                orientação, além disso, a palavra constelação tem origem no latim <b><i>constelattio</i></b>, cujo
                significado é agrupamento de estrelas.</p>
              <p>As constelações tradicionalmente reconhecidas no Ocidente são as 48 que foram adotadas pelos gregos, em
                razão do trabalho do astrônomo Claudio Ptolomeu, no importante tratado Almagesto, um dos mais
                importantes marcos nos estudos da Astronomia.</p>
              <p>Desde então, várias constelações foram adicionadas e outras mudaram de tamanho, até que no começo do
                século XX, em 1922, a União Astronômica Internacional (UAI) estabeleceu o conceito astronômico sobre o
                que são constelações e, por fim, reconheceu e oficializou a nomenclatura de 88 constelações para fins de
                estudo científico.</p>

            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Principais Constelações</h2>
              <p>Entre o grande número de constelações existentes, podemos ressaltar aquelas que são utilizadas para a
                definição do zodíaco. O zodíaco é a área do céu próxima à eclíptica (plano em que o Sol realiza sua
                órbita aparente em relação à Terra), ou seja, é o caminho aparente pelo qual o Sol desloca-se durante o
                período de um ano.</p>
              <p>Nesse caminho, o Sol passa na frente de 12 constelações: Áries, Touro, Gêmeos, Câncer, Leão, Virgem,
                Libra, Escorpião, Sagitário, Capricórnio, Aquário e Peixes. Entretanto, no caminho aparente do Sol, ele
                também passa sobre a constelação de Ophiuccus (Serpentário), que não é reconhecida pelos astrólogos, uma
                vez que a passagem do Sol por ela é breve, de aproximadamente 19 dias.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Ursa Maior</h3>
              <p>Uma das constelações mais famosas do hemisfério celestial norte, também é conhecida em outras partes do
                mundo como O Arado</p>
              <Image src={'/UrsaMaior.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Ursa Menor</h3>
              <p>É uma constelação de forma similar à Ursa Maior, porém, reduzida</p>
              <Image src={'/ursa_menor.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Órion</h3>
              <p>A constelação de Órion fica no equador celeste, é formada por estrelas muito brilhantes como
                Betelgeuse</p>
              <Image src={'/Orion.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Cassiopéia</h3>
              <p>Na mitologia grega, Cassiopeia era uma rainha etíope que comparara sua beleza a beleza das Nereidas
                e, por isso, fora castigada</p>
              <Image src={'/cassiopeia.webp'} width={'600'} height={'300'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Cão Maior</h3>
              <p>É uma constelação do hemisfério celestial sul, sua estrela mais brilhante é Sirius: a estrela mais
                brilhante do céu noturno</p>
              <Image src={'/CaoMaior.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Pegasus</h3>
              <p>Essa constelação recebeu o seu nome após o mito grego do cavalo alado</p>
              <Image src={'/Pegasus.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Andrômeda</h3>
              <p>Andrômeda era filha da rainha Cassiopeia, de acordo com a mitologia grega</p>
              <Image src={'/Andromeda.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h3>Aquila</h3>
              <p>A águia é uma constelação do equador celeste, essa constelação representa a águia que carregava os
                raios de Zeus na mitologia grega</p>
              <Image src={'/Aquila.webp'} width={'450'} height={'500'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Constelações do Zodíaco</h2>
              <p>A divisão do caminho em que o Sol aparentemente se move em relação à Terra, em doze partes iguais, foi
                criada pelos astrônomos babilônios entre 1000 a.C e 500 a.C, de forma que cada uma dessas partes durasse
                o equivalente a 30 dias. A essas partes do trajeto do Sol foram atribuídos signos, dessa maneira, os
                babilônios foram responsáveis por criar o primeiro sistema de coordenadas celestial.</p>
              <p>Durante a formulação do zodíaco, os astrônomos babilônios não dividiram o céu exatamente onde se
                iniciava ou se findava uma constelação. Se tivessem feito isso, as divisões dos meses não seria
                homogênea, além disso, eles resolveram omitir a existência de uma grande constelação da eclíptica solar,
                a constelação da serpente, chamada de Ophiuccus.</p>
              <p>Caso fossem considerados agrupamentos menores de estrelas, o número de constelações zodiacais elegíveis
                chegaria a 21. Ademais, do momento da definição do zodíaco até hoje, o plano da órbita de Terra em
                relação ao Sol foi alterado e por isso as constelações visíveis junto à movimentação do Sol não são mais
                as mesmas daquela época.</p>
              <p>A Astrologia moderna atribui os signos ao momento de nascimento das pessoas, de acordo com ela, a
                posição dos astros pode estar relacionada a características pessoais. Confira quais são constelações do
                zodíaco e as datas em que o Sol encontrava-se sobre elas, no momento da definição do zodíaco:</p>
              <p><b>Áries:</b> Entre 21 de março a 20 de abril<br/>
                <b>Touro:</b> Entre 21 de abril e 20 de maio<br/>
                <b>Gêmeos:</b> Entre 21 de maio a 21 de junho<br/>
                <b>Câncer:</b> Entre 22 de junho a 22 de julho<br/>
                <b>Leão:</b> Entre 23 de julho a 22 de agosto<br/>
                <b>Virgem:</b> Entre 23 de agosto a 22 de setembro<br/>
                <b>Libra:</b> Entre 23 de setembro a 22 de outubro<br/>
                <b>Escorpião:</b> Entre 23 de outubro e 21 de novembro<br/>
                <b>Sagitário:</b> Entre 22 de novembro e 21 de dezembro<br/>
                <b>Capricórnio:</b> Entre 22 de dezembro e 19 de janeiro<br/>
                <b>Aquário:</b> Entre 20 de janeiro e 18 de fevereiro<br/>
                <b>Peixes:</b> Entre 19 de fevereiro e 20<br/>
              </p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Classificação das Constelações</h2>
              <p>As constelações são classificadas de acordo com a posição que ela se encontra na abóboda
                celeste:
                <br/>
                <b>Boreais:</b> São aquelas que se localizam no hemisfério celeste norte<br/>
                <b>Austrais:</b> Se localizam no hemisfério celeste sul<br/>
                <b>Zodiacais:</b> Localizadas ao Longo da eclíptica do Sol<br/>
                <b>Equatoriais:</b> São aquelas sobre o equador celeste</p><br/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default Constelacoes
