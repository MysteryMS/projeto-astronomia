import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

const BuracoNegro = () => {
  return (
    <main>
      <title>Curiosidades Gerais Sobre Buracos Negros</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container>
            <Grid justify={'center'} xs={12} direction={'column'} alignItems={'center'}>
              <h1>Curiosidades Gerais Sobre Buracos Negros</h1>
              <ArticleAuthor name={`João Raposo`}/>
            </Grid>
            <Grid jusitfy={'center'} direction={'column'} xs={12}>
              <h2>Como Surgem os Buracos Negros?</h2>
              <p>Um buraco negro é uma região do espaço onde a matéria entrou
                em colapso sobre si mesma. A atração gravitacional é tão forte que
                nada, nem mesmo a luz, pode escapar dele. Os buracos negros
                surgem após o desaparecimento explosivo de certas estrelas
                grandes.</p>
            </Grid>
            <Grid justify={'center'} direction={'column'} xs={12}>
              <h2>O que acontece se algo for sugado pelo buraco negro?</h2>
              <p>O que se acredita é que tudo que é sugado é levado para
                contribuir para a própria estrutura do buraco negro, ele vai
                aumentando, ficando mais massivo. Quando você passa do limite
                [horizonte de eventos], é como se fosse uma queda eterna, você cai
                caindo e nunca chega a lugar nenhum, caso você se aproxime da
                parte mais interna de um buraco negro, onde as energias são maiores,
                você acaba sendo desmembrado em várias partículas. De acordo com
                ele, a única coisa que vai sobrar é a matéria mais fundamental, as
                partículas subatômicas</p>
            </Grid>
            <Grid justify={'center'} direction={'column'}>
              <h2>O Maior Buraco Negro</h2>
              <p>O maior buraco negro do universo mede 34 bilhões de vezes mais
                do que a massa do nosso Sol seu nome é J2157 ele é considerado o
                buraco negro que cresce mais rápido que se tem conhecimento em
                todo o universo. A massa dele é mais ou menos 8.000 vezes maior
                do que Sagittarius A, o buraco negro do centro da nossa galáxia Se
                o buraco negro da nossa Via Láctea quisesse 'engordar' desse jeito,
                ela teria engolido dois terços de todas as estrelas da nossa galáxia</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default BuracoNegro
