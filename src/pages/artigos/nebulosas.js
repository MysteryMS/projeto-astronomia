import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

const Nebulosas = () => {
  return (
    <main>
      <title>Nebulosas</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} justify={'center'} direction={'column'}>
              <h1>Nebulosas</h1>
              <ArticleAuthor name={"Yasmin Sampaio"} image={'/pfps/yasmin.jpeg'}/>
            </Grid>
            <Grid xs={12} direction={'column'}>
              <p>Grandes nuvens encontradas no espaço interestelar, formadas de poeira cósmica e gases. Algumas, surgem
                com a supernova
                (explosão de estrelas mortas), que consiste no lançamento da matéria da estrela, formando assim, uma ou
                mais nebulosa com
                diferentes formatos e tamanhos. Ou também, pela aglutinação de átomos na ação da gravidade.
                Quando observadas, apresentam formas irregulares, por isso o nome de nebulosas (nuvens).</p>
            </Grid>
            <Grid xs={12}>
              <h2>Tipos de Nebulosas</h2>
            </Grid>
            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Nebulosas de emissão:</b> São grandes massas de gases estimuladas devido as altas temperaturas.
                Iluminadas por luz ultravioletas,
                vindo de estrelas próximas. Geralmente, esse tipo de nebulosa apresenta cor vermelha, por causa do
                hidrogênio.
              </p>
            </Grid>
            <Grid md={6} alignItems={'center'} justify={'center'} xs={12}>
              <Image src={'/nebulosa_emissao.webp'} showSkeleton={false}/>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Nebulosas de reflexão:</b> Não são quentes o suficiente para ter a ionização do gás que a compõe,
                com isso, são visíveis apenas se
                refletir luz sobre elas. São classificadas como refletoras, e geralmente, apresentam tons de azul.</p>
            </Grid>
            <Grid md={6} xs={12} alignItems={'center'}>
              <Image src={'/nebulosa_reflexao.webp'} showSkeleton={false} width={300} height={195} objectFit={'cover'}/>
            </Grid>

            <Grid md={6} xs={12} justify={'center'} alignItems={'center'}>
              <p>
                <b>Nebulosas escuras:</b> Produz regiões escuras que apresentam contraste com o brilho das estrelas. São
                bastante visíveis até mesmo
                na Terra. Elas praticamente impedem a passagem da luz.
              </p>
            </Grid>
            <Grid md={6} xs={12}>
              <Image src={'/nebulosa_escura.webp'} showSkeleton={false} objectFit={'cover'} width={300} height={195}/>
            </Grid>

            <Grid md={6} xs={12} alignItems={'center'}>
              <p><b>Nebulosas planetárias:</b> tem origem na expulsão da massa das estrelas gigantes vermelhas, com
                massas
                intermediárias. Quando
                observadas pela primeira vez, podem ser confundidas com planetas. Representam o estágio final da vida de
                uma estrela.</p>
            </Grid>
            <Grid md={6} xs={12}>
              <Image src={'/nebulosa_planetaria.webp'} showSkeleton={false} objectFit={'cover'} width={300}
                     height={195}/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default Nebulosas
