import * as React from "react"
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'


const galaxia = () => {
  return (
    <main>
      <title>Galáxias</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid justify={'center'} xs={12} direction={'column'}>
              <h1>Galáxias</h1>
              <ArticleAuthor name={'Maria Eduarda Carvalho de Meneses'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Nós sempre ouvimos esse nome, galáxias. Vemos lindas fotos das mesmas e até as usamos de forma poética em músicas. Mas afinal, o que são galáxias? </p>
            </Grid>
             <Grid xs={12} alignItems={'center'} justify={'center'}>
              <Image src={'/galaxia.webp'} showSkeleton={false} width={500} height={350} objectFit={'cover'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <p>Galáxias são um conjunto de estrelas, planetas, gás e poeira cósmica, todos laceados pela gravidade. Um “pequeno” universo dentro de outro muito maior. </p>
              <p>Alguns cientistas concluem que, após o Big Bang, o universo era composto de radiação e partículas subatômicas, e que depois da explosão, essas partículas lentamente começaram e se juntar, formando estrelas, aglomerados de estrelas e por fim, as galáxias. </p>
              <p>O processo evolutivo de uma galáxia ocorre de forma isolada ou agrupada, passando por processos de interação e até mesmo colisão, o que dá origem a supernovas, estrelas e novos buracos negros</p>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <p>Existe três tipos de galáxias: elípticas, espirais e irregulares. </p>
              <Image src={'/galaxias-elipticas.webp'} showSkeleton={false} width={500} height={350} objectFit={'cover'}/>
              <p>A galáxia que vivemos, a Via Láctea, é uma espiral. São as mais comuns, correspondendo dois terços de todas as galáxias conhecidas. Sua forma é semelhante à de um disco e são encontradas em uma cor branca-azulada. São compostas por gases, poeiras e estrela, havendo um intenso processo de formação de novos astros no seu interior.</p>
            </Grid>



            <Grid xs={12} direction={'column'}>
              <Image src={'/galaxias-espirais.webp'} showSkeleton={false} width={500} height={350} objectFit={'cover'}/>
              <p>As galáxias elípticas possuem uma forma oval e achatadas. São compostas por uma quantidade menor de poeira e estrelas comparadas as ouras galáxias. Possuem estrelas muito antigas e quase não atividade de formação de novos astros.</p>
            </Grid>


            <Grid xs={12} direction={'column'}>
              <Image src={'/galaxias-irregulares.webp'} showSkeleton={false} width={500} height={350} objectFit={'cover'}/>
              <p>As galáxias irregulares não têm um formato definido, são formações muito antigas que um dia já foram elípticas e espirais. São compostas por poeira e gases, e tem esse formato irregular por causa da influência do campo gravitacional de outras galáxias em sua proximidade. De acordo com a Nasa, as galáxias irregulares eram o tipo de galáxia mais abundante no início da formação do universo, as que são observadas hoje em dia são muito antigas.</p>

            </Grid>

            <Grid xs={12} direction={'column'}>
              <Image src={'/galaxias.webp'} showSkeleton={false} width={500} height={350} objectFit={'cover'}/>
              <p>Há tantas galáxias no universo que nem tem como contá-las. O telescópio Hubble observou o céu por apenas 12 dias e já foi o suficiente para encontra 10.000 galáxias diferentes. Todas com milhões de estrelas e outros astros dentro delas.</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default galaxia
