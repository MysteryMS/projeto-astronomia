import * as React from 'react'
import Navbar from "../../components/Navbar"
import {Container, Grid, Image, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"
import ArticleAuthor from "../../components/ArticleAuthor"
import '../../article.css'

const Eclipse = () => {
  return (
    <main>
      <title>Eclipse</title>
      <Navbar/>
      <NextUIProvider theme={theme}>
        <Container>
          <Grid.Container gap={1}>
            <Grid xs={12} direction={'column'}>
              <h1>Eclipse</h1>
              <ArticleAuthor name={'Felipe Pelizzer'} image={'/pfps/felipe_pelizzer.jpeg'}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>O que é Eclipse?</h2>
              <p>Eclipse é um fenômeno astronômico marcado pelo escurecimento total ou parcial de um astro feito por
                meio da interposição de um segundo astro frente à fonte de luz. Existem dois tipos de eclipses: o solar
                e o lunar.</p>
              <p>É necessário salientar que tanto o eclipse lunar quanto o solar dependem de um alinhamento das órbitas
                da Terra, ao redor do Sol, e da Lua, ao redor da Terra; caso contrário, os fenômenos não ocorrerão. </p>
              <p>Os eclipses mais conhecidos são os eclipses do Sol e da Lua. Apesar disso, também é possível observar
                eclipses de satélites de outros planetas, como Júpiter e Saturno, por exemplo.</p>
              <p>Eclipse é uma palavra do âmbito da astronomia que significa o desaparecimento temporário de um
                astro.</p>
              <p>Os eclipses, há milênios, são considerados como pontos de mudanças, crises e até possíveis calamidades.
                No passado, eles causavam muito medo entre as pessoas, por isso eram realizados rituais para poder
                "exorcizar" os seus efeitos do mal.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Eclipse Solar</h2>
              <p>Ocorre o eclipse solar quanto a lua interpõe-se entre o Sol e a Terra, projetando a sua sombra sobre a
                Terra. Nas regiões do planeta onde o Sol é observado sendo completamente tampado pela Lua, ocorre o
                chamado eclipse solar total. Tais regiões encontram-se na posição da sombra da Lua. Nos lugares onde o
                sol não fica completamente encoberto pela lua, ocorre o eclipse solar parcial, correspondendo às regiões
                de penumbra da Lua. Caso a órbita da Lua ao redor da Terra fosse alinhada com a órbita da Terra ao redor
                do Sol, sempre teríamos eclipse solar durante a fase da Lua Nova. </p>
              <p>Os eclipses solares são graduais, ou seja, a Lua leva certo tempo para poder cobrir o Sol, por isso é
                comum vermos fotos dos estágios de um eclipse</p>
              <Image src={'/eclipse1.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Tipos de Eclipses Solares</h2>
              <ul>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses solares totais:</span> a Lua cobre
                  totalmente a luz solar, projetando sua sombra sobre a Terra;</p></li>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses solares parciais:</span> a Lua não fica
                  perfeitamente alinhada com o Sol, cobrindo somente parte de sua luminosidade;</p></li>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses Solares Anulares:</span> o tamanho
                  aparente da Lua não é suficiente para cobrir totalmente a luz solar. Esse fenômeno resulta no
                  surgimento de um anel em volta da sombra da Lua. </p></li>
              </ul>
              <Image src={'/tipos_eclipse.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Eclipse Lunar</h2>
              <p>O eclipse lunar ocorre quando a Lua entra na região da sombra da Terra, gerada por meio da luz do Sol,
                e a sombra da Terra cobre o disco lunar. Caso a órbita da Lua ao redor da Terra fosse alinhada com a
                órbita da Terra ao redor do Sol, sempre teríamos eclipse lunar durante a fase da Lua Cheia.</p>
              <Image src={'/eclipse_lunar.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Tipos de Eclipses Lunares</h2>
              <p>Existem os eclipses lunares penumbrais, parciais e totais. Todos esses tipos de eclipse estão
                relacionados à posição relativa do observador na Terra.</p>
              <ul>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses Penumbreais:</span> a superfície da lua
                  fica
                  levemente escurecida ao atravessar a
                  região do cone de penumbra produzido pela Terra</p></li>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses Lunares Parciais:</span> somente parte da
                  sombra da Terra é projetada sobre a Lua</p></li>
                <li><p>• <span style={{textDecoration: 'underline'}}>Eclipses Totais:</span> toda a superfície lunar é
                  coberta pela sombra da Terra.</p></li>
              </ul>
              <Image src={'/fases_eclipse_lunar.webp'} showSkeleton={false}/>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Curiosidades</h2>
              <ul>
                <li><p>• A palavra eclipse tem origem no grego ékleipsis</p></li>
                <li><p>• No ano de 2020 ocorreu dois eclipses solares, porém, nenhum deles foi visível no Brasil. O
                  primeiro ocorreu em 21 de junho, que foi um eclipse anular do Sol. O segundo ocorreu em 14 de dezembro
                  de 2020, que foi um eclipse solar total (visível no sul da Argentina e Chile).</p></li>
                <li><p>• O primeiro registro de eclipse solar da história foi realizado em 763 a.C, na Assíria.</p>
                </li>
                <li><p>• Na antiguidade, em função do baixo conhecimento astronômico, muitos povos acreditavam que o
                  eclipse era um sinal de que alguma coisa ruim ou uma catástrofe natural estava por acontecer.</p>
                </li>
                <li><p>• A NASA fornece calendários das ocorrências de eclipses todo ano.</p></li>
                <li><p>• Ao observar as fases da Lua, é possível imaginar que a cada Lua Nova ocorra um eclipse
                  solar.</p></li>
              </ul>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Cuidado!</h2>
              <p>Olhar diretamente para o Sol durante um eclipse pode causar queima da retina ou outros problemas
                oculares sérios. Para visualizar este fenômeno é necessário a utilização de óculos projetados
                especificamente para este caso. Óculos de sol comum, papel celofane, filme fotográfico ou qualquer outro
                objeto não devem ser utilizados em dias de eclipse solar.</p>
            </Grid>

            <Grid xs={12} direction={'column'}>
              <h2>Conteúdo Relacionado</h2>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/RIMYYv05JzM"
                      title="YouTube video player" frameBorder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen></iframe>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}
//TODO: add sources
export default Eclipse
