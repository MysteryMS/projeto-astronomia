import * as React from "react"
import "../index.css"
import {StaticImage} from "gatsby-plugin-image"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../Theme"
import articles from "../articles.json"
import Article from "../components/Article"
import Navbar from "../components/Navbar"

const IndexPage = () => {
  return (
    <main>
      <title>Astronomia – Página Inicial</title>
      <Navbar fixed={true}/>
      <NextUIProvider theme={theme}>
        <div style={{marginBottom: '11px'}}>
          <StaticImage src={'../images/header5.webp'} layout={'fullWidth'} alt={'Banner'}/>
        </div>
        <h2 style={{textAlign: 'center', fontSize: '38px', fontWeight: '500', marginBottom: '42px'}}>🌠 Início</h2>
        <div style={{marginBottom: '50px'}}>
          <Container>
            <Grid.Container gap={1.5} justify="center">
              {articles.articles.map(article => {
                return (
                  <Grid xs={12} md={4} lg={4} sm={6} justify="center">
                    <Article props={article}/>
                  </Grid>
                )
              })}
            </Grid.Container>
          </Container>
        </div>
      </NextUIProvider>

    </main>
  )
}

export default IndexPage
