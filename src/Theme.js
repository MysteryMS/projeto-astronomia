import {createTheme} from '@nextui-org/react'

export const theme = createTheme({
  type: 'dark',
  theme: {
    colors: {
      background: '#222222'
    }
  }
})
