import {Button, Card, Col, Row, Text} from "@nextui-org/react"
import * as React from "react"
import {navigate} from "gatsby"
import hbo from "../images/hbo.webp"

const Interestelar = () => {
  return (
    <Card cover css={{maxHeight: 400, maxWidth: 280}}>
      <style> {'.nextui-c-bVLlAJ-cCitdK-ready-false { opacity: 1 !important; }'} </style>
      <Card.Image showSkeleton={false}
                  src={'/interestelar.webp'}/>
      <Card.Footer blur css={{
        position: 'absolute',
        bgBlur: '#0f1114',
        borderTop: '$borderWeights$light solid $gray700',
        bottom: 0,
        zIndex: 1
      }}>
        <Row align={'center'}>
          <Col>
            <Card.Image src={hbo} showSkeleton={false} height={40} width={40}/>
          </Col>
          <Col>
            <Text size={14}>Interestelar</Text>
          </Col>
        </Row>
        <Col>
          <Row justify="flex-end">
            <Button flat auto rounded css={{ color: '#ffffff', bg: '#ab43f8' }} onClick={() => {navigate("https://play.hbomax.com/page/urn:hbo:page:GYGP7pwQv_ojDXAEAAAFc:type:feature")}}>
              <Text css={{ color: 'inherit' }} size={12} weight="bold" transform="uppercase">
                Assistir
              </Text>
            </Button>
          </Row>
        </Col>
      </Card.Footer>
    </Card>
  )
}

export default Interestelar
