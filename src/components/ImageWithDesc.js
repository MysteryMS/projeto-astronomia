import {Grid, Image} from "@nextui-org/react"
import * as React from 'react'

const ImageWithDesc = ({image, description, w, h}) => {
  return (
    <Grid justify={'center'} alignItems={'center'} direction={'column'} xs={12}>
      <Image src={image} showSkeleton={false} width={w ? w : ''} height={h ? h : ''}/>
      <p style={{fontSize: '15px'}}><i>{description}</i></p>
    </Grid>
  )
}

export default ImageWithDesc
