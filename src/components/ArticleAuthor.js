import * as React from "react"
import {Avatar, Spacer} from "@nextui-org/react"

const ArticleAuthor = ({name, image}) => {
  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: '35px',
      marginTop: '10px'
    }}>
      {image ? <Avatar squared src={image}/> : <Avatar squared text={name[0]}/>}
      <Spacer x={1}/>
      <p style={{fontWeight: 300, fontSize: '25px'}}>{name}</p>
    </div>
  )
}

export default ArticleAuthor
