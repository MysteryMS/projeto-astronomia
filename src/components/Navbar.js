import * as React from "react"
import {Link} from "gatsby"
import {StaticImage} from "gatsby-plugin-image"
import {Link as NLink} from "@nextui-org/react"

const navbarDiv = {
  display: "flex",
  justifyContent: "space-between",
  direction: "row",
  paddingLeft: '30px',
  paddingRight: '50px',
  width: '100vw',
  alignItems: 'center',
  zIndex: '1',
  height: '80px',
  backdropFilter: 'blur(8px)',
  background: 'rgba(0,0,0,0.30)',
  boxShadow: '0 4px 30px rgba(0, 0, 0, 0.1)',
}

const navLink = {
  fontSize: '20px',
  color: '#FFFFFF',
  display: 'flex',
  marginLeft: '40px'
}

const Navbar = ({fixed}) => {
  const novaNavbarDiv = {
    ...navbarDiv,
    position: fixed ? 'fixed' : 'relative'
  }
  return (
    <div style={novaNavbarDiv}>
      <div style={{display: 'flex', alignItems: 'center'}}>
        <Link to={'/'}>
          <StaticImage src={'../images/logo1.webp'} alt={'logo'} width={60} height={60} placeholder={'blurred'}/>
        </Link>
      </div>
      <div style={{display: 'flex', alignItems: 'center'}}>
        <div style={navLink}><Link style={{color: 'white'}} to={'/'}>Início</Link></div>
        <div style={navLink}><NLink css={{color: 'white'}} href={'https://etecitapira.com.br'} icon>Site Etec</NLink></div>
        <div style={navLink}><Link style={{color: 'white'}} to={'/sobre'}>Sobre</Link></div>
      </div>
    </div>
  )
}

export default Navbar
