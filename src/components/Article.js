import React from 'react'
import {Card} from "@nextui-org/react"
import {navigate} from "gatsby"

const Article = ({props}) => {
  return (
    <Card clickable bordered css={{maxWidth: 400, maxHeight: 450}} onClick={() => {
      navigate(props.path)
    }}>
      <style> {'.nextui-c-bVLlAJ-cCitdK-ready-false { opacity: 1 !important; }'} </style>
      <Card.Image
        src={`/${props.header}`}
        alt={'card img bkg'} width={400} height={170} objectFit={'cover'}/>
      <Card.Body>
        <h3 style={{textAlign: 'center'}}>{props.title}</h3>
        <p style={{textAlign: 'justify', fontSize: '17px', overflow: 'hidden'}}>{props.description}</p>
      </Card.Body>
    </Card>
  )
}

export default Article
