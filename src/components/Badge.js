import * as React from 'react'

const Badge = ({text, color}) => {
  return(
    <div style={{backgroundColor: color, borderRadius: '100px', maxWidth: '350px', display: 'flex', justifyContent: 'center', alignItems: 'center', padding: '10px 10px 10px 10px', maxHeight: '25px'}}>
      <span style={{fontWeight: 'bold'}}>{text}</span>
    </div>
  )
}

export default Badge
