import {Button, Card, Col, Row, Text} from "@nextui-org/react"
import * as React from "react"
import {navigate} from "gatsby"
import poster from "../images/UniverseSandbox_poster.webp"
import steam from "../images/steam.webp"

const UniverseSandbox = () => {
  return (
    <Card cover css={{maxHeight: 400, maxWidth: 280}}>
      <style> {'.nextui-c-bVLlAJ-cCitdK-ready-false { opacity: 1 !important; }'} </style>
      <Card.Image showSkeleton={false}
                  src={poster}/>
      <Card.Footer blur css={{
        position: 'absolute',
        bgBlur: '#323232',
        borderTop: '$borderWeights$light solid $gray700',
        bottom: 0,
        zIndex: 1
      }}>
        <Row align={'center'}>
          <Col>
            <Card.Image src={steam} showSkeleton={false} height={40} width={40}/>
          </Col>
          <Col>
            <Text size={14}>Universe Sandbox</Text>
          </Col>
        </Row>
        <Col>
          <Row justify="flex-end">
            <Button flat auto rounded css={{ color: '#ffffff', bg: '#595253' }} onClick={() => {navigate("https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjlnra-0JP3AhUoLLkGHVUkAdEQFnoECBEQAQ&url=https%3A%2F%2Fstore.steampowered.com%2Fapp%2F361420%2FASTRONEER%2F&usg=AOvVaw1-GUlYDWpwUGT3WvKhOI8I")}}>
              <Text css={{ color: 'inherit' }} size={12} weight="bold" transform="uppercase">
                Jogar
              </Text>
            </Button>
          </Row>
        </Col>
      </Card.Footer>
    </Card>
  )
}

export default UniverseSandbox
