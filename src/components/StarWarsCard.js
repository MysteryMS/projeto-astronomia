import {Button, Card, Col, Row, Text} from "@nextui-org/react"
import * as React from "react"
import {navigate} from "gatsby"
import poster from "../images/sw_poster.webp"
import disney from "../images/disney.webp"

const StarWarsCard = () => {
  return (
    <Card cover css={{maxHeight: 400, maxWidth: 280}}>
      <style> {'.nextui-c-bVLlAJ-cCitdK-ready-false { opacity: 1 !important; }'} </style>
      <Card.Image showSkeleton={false}
                  src={poster}/>
      <Card.Footer blur css={{
        position: 'absolute',
        bgBlur: '#193969',
        borderTop: '$borderWeights$light solid $gray700',
        bottom: 0,
        zIndex: 1
      }}>
        <Row align={'center'}>
          <Col>
            <Card.Image src={disney} showSkeleton={false} height={40} width={40}/>
          </Col>
          <Col>
            <Text size={14}>Star Wars</Text>
          </Col>
        </Row>
        <Col>
          <Row justify="flex-end">
            <Button flat auto rounded css={{ color: '#ffffff', bg: '#035397' }} onClick={() => {navigate("https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwitucqS4JL3AhWzQkgAHbgkCsYYABAAGgJjZQ&ohost=www.google.com&cid=CAESa-D2ACs3wcW3ojz1r5VUPCeYRyNuwlXe2UYOdJrNRwu8Qj58avDiw0QnDTCGQXcFTH287pDEg6jaPT-yIX5LfM1s58lnRRbg9_rfY7MtaCWq9HoTrofGuRQhkW-D5EdSvextAFhfZSpGGnog&sig=AOD64_3iWGGm6XU1VkT-6UxzX2TBZa9UxA&q&adurl&ved=2ahUKEwj-xsOS4JL3AhVmK7kGHTlfDhYQ0Qx6BAgCEAE")}}>
              <Text css={{ color: 'inherit' }} size={12} weight="bold" transform="uppercase">
                Assistir
              </Text>
            </Button>
          </Row>
        </Col>
      </Card.Footer>
    </Card>
  )
}

export default StarWarsCard
